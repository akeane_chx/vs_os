#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <vsos/vsfs.h>

int param_verbose = 1;

int vsfs_read_dir_entry(struct vsfs_dir *cwd, int entry)
{
	struct vsfs_dirent *dirent;
	struct vsfs_inode *inode;

	if (entry >= VSFS_DIRENT_MAX)
		return -EINVAL;

	dirent = &(cwd->dirent[entry]);
	inode = &(dirent->inode);

	printf("name: \"%s\" ", dirent->name);

	switch (inode->type) {
	case VSFS_TYPE_BLOCK:
		printf("type: block ");
		break;
	case VSFS_TYPE_CHAR:
		printf("type: char ");
		break;
	case VSFS_TYPE_DIR:
		printf("type: directory ");
		break;
	case VSFS_TYPE_DIREXT:
		printf("type: extended directory ");
		break;
	case VSFS_TYPE_REG:
		printf("type: regular file ");
		break;
	case VSFS_TYPE_REGEXT:
		printf("type: extended regular file ");
		break;
	default:
		return -EINVAL;
	}

	printf("block: %d\n", inode->block);

	return 0;
}

int block_unused_get(uint8_t *block_in_use, int blocks)
{
	int i = 0;

	for (i = 0; i < blocks; i++) {
		if (!block_in_use[i])
			return i;
	}

	return -ENOSPC;
}

int block_read(int f, char *block, int blocks, int blocksize)
{
	int i = 0;

	for (i = 0; i < blocks; i++) {
		if (read(f, &block[i * blocksize], blocksize) < 0)
			return -1;
	}

	return i;
}

int main(int argc, char *argv[])
{
	int cwd_block = 0, i = 0;
	struct vsfs_inode cwd_inode;
	unsigned char block[VSFS_BLOCK_SIZE * VSFS_MAX_BLOCKS];
	uint8_t block_in_use[VSFS_BLOCK_SIZE * VSFS_MAX_BLOCKS];
	struct vsfs_dir *root_dir;
	struct vsfs_superblock *superblock;

	memset(&cwd_inode, 0, sizeof(struct vsfs_inode));
	memset(block, 0, sizeof(block));
	memset(block_in_use, 0, sizeof(block_in_use));

	/* Read in first block */
	if (!block_read(0, block, 1, VSFS_BLOCK_SIZE)) {
		fprintf(stderr, "Error reading superblock\n");
		return 1;
	}

	superblock = (struct vsfs_superblock *)&(block[0]);

	if (superblock->id == VSFS_ID) {
		printf("Found vsfs superblock id: 0x%04x\n", superblock->id);
	} else {
		fprintf(stderr, "Error reading superblock\n");
		return 1;
	}

	printf("Blocks   : %d\n", superblock->blocks);
	printf("Blocksize: %d\n", superblock->block_size);
	printf("Root inode points to block: %d type: 0x%04x\n",
	       superblock->i_root.block, superblock->i_root.type);

	printf("Reading remaining blocks : %d\n", superblock->blocks - 1);

	if (!block_read
	    (0, &block[superblock->block_size], superblock->blocks - 1,
	     superblock->block_size)) {
		fprintf(stderr, "Error reading blocks\n");
		return 1;
	}
	//memcpy(&(superblock->i_root), &cwd_inode, sizeof(struct vsfs_inode));

	root_dir =
	    (struct vsfs_dir *)&block[superblock->block_size *
				      superblock->i_root.block];

	printf("Found %d entries\n", root_dir->entries);

	for (i = 0; i < root_dir->entries; i++) {
		if (vsfs_read_dir_entry(root_dir, i) < 0) {
			fprintf(stderr, "Error reading entry : %d\n", i);
			return 1;

		}
	}

	return 0;
}
