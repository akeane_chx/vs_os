#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <vsos/vsfs.h>

int param_verbose = 1;

int vsfs_add_dir_entry(struct vsfs_dir *cwd, char *name,
		       struct vsfs_inode *inode)
{
	struct vsfs_dirent *dirent;

	if (param_verbose) {
		fprintf(stderr, "Adding: %s type: 0x%04x len: %d\n", name,
			inode->type, inode->len);
	}

	if (cwd->entries == (VSFS_DIRENT_MAX - 1))
		return -ENOSPC;

	switch (inode->type) {
	case VSFS_TYPE_BLOCK:
		break;
	case VSFS_TYPE_CHAR:
		break;
	case VSFS_TYPE_DIR:
		break;
	case VSFS_TYPE_DIREXT:
		break;
	case VSFS_TYPE_REG:
		break;
	case VSFS_TYPE_REGEXT:
		break;
	default:
		return -EINVAL;
	}

	dirent = &(cwd->dirent[cwd->entries]);
	strncpy(dirent->name, name, VSFS_NAME_MAX);
	memcpy(&(dirent->inode), inode, sizeof(inode));
	cwd->entries++;

	if (param_verbose)
		fprintf(stderr, "dir entries : %d\n", cwd->entries);

	return 0;
}

int block_unused_get(uint8_t *block_in_use, int blocks)
{
	int i = 0;

	for (i = 0; i < blocks; i++) {
		if (!block_in_use[i]) {
			block_in_use[i] = 1;
			return i;
		}
	}

	return -ENOSPC;
}

int block_write(int f, char *block, int blocks, int blocksize)
{
	int i = 0;

	for (i = 0; i < blocks; i++) {
		if (write(f, &block[i * blocksize], blocksize) < 0)
			return -1;
	}

	return i;
}

int main(int argc, char *argv[])
{
	int cwd_block = 0;
	struct vsfs_inode cwd_inode, file_inode, root_inode;
	struct vsfs_inode console_inode;
	unsigned char block[VSFS_BLOCK_SIZE * VSFS_MAX_BLOCKS];
	uint8_t block_in_use[VSFS_BLOCK_SIZE * VSFS_MAX_BLOCKS];
	struct vsfs_dir *root_dir, *cwd_dir;
	struct vsfs_superblock *superblock;

	char file_data[VSFS_BLOCK_SIZE] = "This is some file data\n";

	memset(&cwd_inode, 0, sizeof(struct vsfs_inode));
	memset(&file_inode, 0, sizeof(struct vsfs_inode));
	memset(&root_inode, 0, sizeof(struct vsfs_inode));
	memset(block, 0, sizeof(block));
	memset(block_in_use, 0, sizeof(block_in_use));

	superblock = (struct vsfs_superblock *)&(block[0]);

	block_in_use[0] = 1;

	cwd_block = block_unused_get(block_in_use, sizeof(block_in_use));

	if (cwd_block <  0) {
		fprintf(stderr, "Can't find empty block\n");
		return 1;
	}

	root_dir = (struct vsfs_dir *)&(block[cwd_block * VSFS_BLOCK_SIZE]);

	/* Setup root inode */

	cwd_inode.type = VSFS_TYPE_DIR;
	cwd_inode.major = 0;
	cwd_inode.minor = 0;
	cwd_inode.len = sizeof(struct vsfs_dir);
	cwd_inode.perm = 0;
	cwd_inode.block = cwd_block;

	root_inode.type = VSFS_TYPE_DIR;
	root_inode.major = 0;
	root_inode.minor = 0;
	root_inode.len = sizeof(struct vsfs_dir);
	root_inode.perm = 0;
	root_inode.block = cwd_block;

	if (vsfs_add_dir_entry(root_dir, ".", &root_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", ".");
		return 1;
	}

	if (vsfs_add_dir_entry(root_dir, "..", &root_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}

	cwd_block = block_unused_get(block_in_use, sizeof(block_in_use));
	if (cwd_block < 0) {
		fprintf(stderr, "Can't find empty block\n");
		return 1;
	}

	fprintf(stderr, "dev points to block %d\n", cwd_block);

	cwd_dir = (struct vsfs_dir *)&(block[cwd_block * VSFS_BLOCK_SIZE]);
	cwd_inode.block = cwd_block;

	if (vsfs_add_dir_entry(root_dir, "dev", &cwd_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "dev");
		return 1;
	}

	file_inode.type = VSFS_TYPE_REG;
	file_inode.major = 0;
	file_inode.minor = 0;
	file_inode.len = strnlen(file_data, VSFS_BLOCK_SIZE);
	file_inode.perm = 0;
	file_inode.block = block_unused_get(block_in_use, sizeof(block_in_use));
	fprintf(stderr, "file block: %d\n", file_inode.block);

	strncpy(&(block[file_inode.block * VSFS_BLOCK_SIZE]), file_data, VSFS_BLOCK_SIZE);

	if (vsfs_add_dir_entry(root_dir, "file", &file_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "dev");
		return 1;
	}

	if (vsfs_add_dir_entry(cwd_dir, ".", &cwd_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", ".");
		return 1;
	}

	if (vsfs_add_dir_entry(cwd_dir, "..", &root_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}

	/* Setup dev inodes */

	console_inode.type = VSFS_TYPE_CHAR;
	console_inode.major = 0;	/* Copy Linux */
	console_inode.minor = 0;	
	console_inode.len = 0;
	console_inode.perm = 0;
	console_inode.block = -1;	/* more defining */

	if (vsfs_add_dir_entry(cwd_dir, "null", &console_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}

	console_inode.type = VSFS_TYPE_CHAR;
	console_inode.major = 2;        /* simple console with no tty ioctl */
	console_inode.minor = 0;	/* USART2 */
	console_inode.len = 0;
	console_inode.perm = 0;
	console_inode.block = -1;	/* more defining */

	if (vsfs_add_dir_entry(cwd_dir, "console", &console_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}

	console_inode.type = VSFS_TYPE_CHAR;
	console_inode.major = 3;        /* tty device */
	console_inode.minor = 0;	/* USART2 */
	console_inode.len = 0;
	console_inode.perm = 0;
	console_inode.block = -1;	/* more defining */

	if (vsfs_add_dir_entry(cwd_dir, "ttyS0", &console_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}
	
	console_inode.type = VSFS_TYPE_CHAR;
	console_inode.major = 1;	
	console_inode.minor = 0;
	console_inode.len = 0;
	console_inode.perm = 0;
	console_inode.block = -1;	/* more defining */

	if (vsfs_add_dir_entry(cwd_dir, "zero", &console_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}

	console_inode.type = VSFS_TYPE_CHAR;
	console_inode.major = 4;	/* virtual process file */
	console_inode.minor = 0;
	console_inode.len = 0;
	console_inode.perm = 0;
	console_inode.block = -1;	/* more defining */

	if (vsfs_add_dir_entry(cwd_dir, "sysreq", &console_inode) < 0) {
		fprintf(stderr, "Can't add dirent: %s\n", "..");
		return 1;
	}

	/* setup superblock */

	superblock->id = VSFS_ID;
	superblock->blocks = 4;
	superblock->block_size = VSFS_BLOCK_SIZE;
	memcpy(&(superblock->i_root), &root_inode, sizeof(struct vsfs_inode));

	block_write(1, block, superblock->blocks, VSFS_BLOCK_SIZE);

	return 0;
}
