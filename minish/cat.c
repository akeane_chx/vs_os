#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

int minish_cat(int argc, char *argv[])
{
	int fd = 0;
	int n = 0;
	char file_data[512] = "";

	char pathname[PATH_MAX] = "";
	char path[PATH_MAX];

	if (argc == 1)
		return -1;
	if (argc == 2)
		strncpy(pathname, argv[1], PATH_MAX);

	/* Absolute pathname ? */
	if(pathname[0] != '/') {
		getcwd(path, PATH_MAX);
		strncat(path, "/", PATH_MAX);
		strncat(path, pathname, PATH_MAX);
	} else {
		strncpy(path, pathname, PATH_MAX);
	}
	
	fd = open(path, O_RDONLY);

	if (fd < 0) {
		printf("cat : cannot open \'%s\': errno (%d)\n", pathname, errno);
		return -1;
	}

	n = read(fd, file_data, 512);

	if(n < 0) {
		printf("cat : error reading \'%s\': errno (%d)\n", pathname, errno);
		close(fd);
		return -1;
	}
	
	printf("%s", file_data);

	close(fd);
	return 0;
}
