#include <stdio.h>
#include <vsos/block.h>
#include <vsos/mount.h>

int minish_mount(int argc, char *argv[])
{
	int i = 0;
	struct mount *mount = 0;
	struct block_dev *bdev = 0;

	for (i = 0; i < MOUNT_MAX; i++) {
		mount = mount_get_entry(i);
		if (mount) {
			bdev = block_dev_get(mount->bdev);
			if (bdev)
				printf("%s", (bdev->name));
			else
				printf("(none)");
			printf(" on %s \t %s \t flags=%d\n", mount->name, mount->type, mount->flags);
		} else
			return 0;
	}
	
	return 0;
}
