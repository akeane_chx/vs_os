#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>

int minish_ls(int argc, char *argv[])
{
	struct fs_dirent *dirent;
	DIR *dirp;
	char pathname[PATH_MAX] = "";
	char path[PATH_MAX];

	if (argc == 1)
		strncpy(pathname, ".", PATH_MAX);
	if (argc == 2)
		strncpy(pathname, argv[1], PATH_MAX);

	/* Absolute pathname ? */
	if(pathname[0] != '/') {
		getcwd(path, PATH_MAX);
		strncat(path, "/", PATH_MAX);
		strncat(path, pathname, PATH_MAX);
	} else {
		strncpy(path, pathname, PATH_MAX);
	}
	
	dirp = opendir(path);

	if (!dirp) {
		printf("ls : cannot access \'%s\': errno (%d)\n", pathname, errno);
		return -1;
	}

	dirent = readdir(dirp);
	if(!dirent) {
		printf("ls : cannot access \'%s\': errno (%d)\n", pathname, errno);
		closedir(dirp);
		return -1;
	}

	fs_print_dirent(dirent);
	printf("\n");
	
	while ((dirent = readdir(dirp))) {
		fs_print_dirent(dirent);
		printf("\n");
	}
	closedir(dirp);
	
	return 0;
}
