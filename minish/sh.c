#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <minish.h>

#define OS_STR "vsOS"
#define OS_VER "0.1"

#define CMD_EXIT "exit"
#define CMD_LS "ls"
#define CMD_CAT "cat"
#define CMD_MOUNT "mount"
#define CMD_PWD "pwd"

static int minish_cmd_num;
static struct minish_cmd minish_cmd[MINISH_CMD_MAX];

int minish_add_cmd(char *name, int (*func) (int argc, char *argv[]))
{
	if (minish_cmd_num >= MINISH_CMD_MAX)
		return -1;

	strncpy(minish_cmd[minish_cmd_num].name, name, NAME_MAX);
	minish_cmd[minish_cmd_num].func = func;
	minish_cmd_num++;
	return 0;
}

struct minish_cmd *minish_get_cmd(char *str)
{
	int i = 0;

	for (i = 0; i < minish_cmd_num; i++) {
		if (strneq(minish_cmd[i].name, str, NAME_MAX))
			return &minish_cmd[i];
	}

	return 0;
}

int minish_cmd_next(char *cmd, char *element)
{
	int i = 0;
	int e = 0;
	char c = 0;

	if (cmd[i] == 0)
		return 0;

	for (i = 0; i < PATH_MAX; i++) {
		c = cmd[i];

		if (c == ' ') {
			element[e] = 0;
			return i;
		}

		if (c == 0) {
			element[e] = 0;
			return i;
		}

		element[e] = c;
		e++;
	}

	return 0;
}

int minish_cmd_exec(char *cmd)
{
	int len = 0;
	int n = 0;
	int max_len = strnlen(cmd, PATH_MAX);
	char element[FS_NAME_MAX] = "";
	int argc = 0;
	struct minish_arg arg[MINISH_MAX_ARGC];
	char *argv[MINISH_MAX_ARGC];
	struct minish_cmd *minish_cmd = 0;

	memset(arg, 0, sizeof(arg));
	len = minish_cmd_next(&cmd[n], element);

	if (len > 0) {
		minish_cmd = minish_get_cmd(element);
		/* Look for command */
		if (!minish_cmd) {
			printf("%s : command not found\n", element);
			return -1;
		}

		strncpy(arg[argc].arg, element, NAME_MAX);
		argv[argc] = arg[argc].arg;
		argc++;
		n += (len + 1);

		if (n < max_len) {
			while ((len = minish_cmd_next(&cmd[n], element)) > 0) {
				if (argc > MINISH_MAX_ARGC) {
					printf("error : too many args (%d)\n", argc);
					return -1;
				}

				strncpy(arg[argc].arg, element, NAME_MAX);
				argv[argc] = arg[argc].arg;
				argc++;

				n += (len + 1);
				if (n > max_len)
					break;
			}
		}

		/* Do command */
		return minish_cmd->func(argc, argv);

	}

	return 0;
}

/* Should have something more generic than just username like envp */
int minish(char *user_name)
{
	char hostname[80]; /* should define MAX somewhere! */
	char cmd_str[80] = "";
	int i = 0;
	int cmd_count = 1;	/* For testing commands */


	/* until uname is implemented use default */
	strncpy(&(hostname[0]), "hostname", 80);
	
	minish_add_cmd(CMD_PWD, minish_pwd);
	minish_add_cmd(CMD_MOUNT, minish_mount);
	minish_add_cmd(CMD_CAT, minish_cat);
	minish_add_cmd(CMD_LS, minish_ls);

	printf("\n");
	while (1) {
		cmd_str[0] = 0;
		printf("%s", user_name);
		printf("@%s ~/ $ ", hostname);
		gets(cmd_str, sizeof(cmd_str));

		if (!strneq(CMD_EXIT, cmd_str, sizeof(CMD_EXIT))) {
			for (i = 0; i < cmd_count; i++)
				minish_cmd_exec(cmd_str);
		} else {
			return 0;
		}
	}
}
