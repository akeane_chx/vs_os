#include <vsos/errno.h>
#include <vsos/task.h>
#include <lib/string.h>
#include <lib/fcntl.h>
#include <vsos/fs.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>

int do_write(int fd, void *buf, int len)
{
	struct fs_file *file = 0;
	struct fs_ops *ops = 0;
	struct task volatile *task = 0;

        task = task_current_get();

        if(!task)
                return -EPERM;


	if(fd < 0)
		return -EBADF;

	if(fd >= FS_FILE_MAX)
		return -EBADF;

	file = (struct fs_file *) &(task->file[fd]);

        if(!file)
                return -EPERM;

	if(file->access_mode == O_RDONLY)
		return -EBADF;

        ops = task->file[fd].ops;

        if(!ops)
                return -EPERM;

        if(!ops->write)
                return -EPERM;

	return ops->write(file, buf, len);
}

int sys_write(int fd, void *buf, int len)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(fd));
	asm volatile ("mov r1, %[d]"::[d] "r"(buf));
	asm volatile ("mov r2, %[d]"::[d] "r"(len));
	asm volatile ("svc %0"::"I" (SYSCALL_WRITE));

	return svc_ret;
}
