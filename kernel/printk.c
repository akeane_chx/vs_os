#include <lib/stdio.h>
#include <lib/string.h>
#include <vsos/board.h>

/* Only use these funcs in kernel context */
/* Do NOT use them in a task as they will lock up the serial device !!! */

int kputchar(int c)
{
	struct board *board = board_get();
	if(board->putchar)
		board->putchar(c);
	return -1;
}

int kputs(const char *str)
{
	int str_count = 0;

        while (str[str_count]) {
                kputchar(str[str_count]);
                str_count++;
        }

	kputchar((int) '\n');
        return 0;
}

/* puts() adds a newline we don't want that for printf strings */
int kputs_truncated(const char *str)
{
	int i = 0;
	while(str[i]) {
		kputchar((int) str[i]);
		i++;
	}
	
	return 0;
}

int kputx(int val)
{
        char str[32] = "";

        xtoa(str, val);
        return kputs_truncated(str);
}

int kputi(int val)
{
        char str[32] = "";

        itoa(str, val);
        return kputs_truncated(str);
}

int kgetc(void)
{
	struct board *board = board_get();
	if(board->getc);
		return board->getc();

	return -1;
}

#define ARM_FRAME_PREV_STACK_WORD_OFFSET (6)
/* Note may need to use -mapcs-frame CFLAG to enforce this */
int printk(const char *format, ...)
{
	int i = 0;
	/* Get the frame pointer */
	int *frame = __builtin_frame_address(0);
	/* first value at frame pointer is the orginal stack */ 
	int *va_args =  frame;

	va_args += ARM_FRAME_PREV_STACK_WORD_OFFSET;	
	va_args++; /* skip format arg */

	while(format[i]) {
		switch(format[i]) {
			case '%':
				i++;
				if(!format[i])
					return -1;
				switch(format[i]) {
					case '%':
						kputchar((int) '%');
						va_args++;
						i++;
						break;
					case 'c':
						kputchar((int) *va_args);
						va_args++;
						i++;
						break;
					case 'd':
						kputi((int) *va_args);
						va_args++;
						i++;
						break;
					case 'x':
						kputx((int) *va_args);
						va_args++;
						i++;
						break;
					case 's':
						kputs_truncated((char *) *va_args);
						va_args++;
						i++;
						break;
					default:
						return -1;
				}
			default:
				if(!format[i])
					return 0;			
				kputchar(format[i]);
				i++;
		}
	}

	return 0;
}
