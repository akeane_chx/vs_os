#include <lib/string.h>
#include <vsos/mount.h>

int mount_num;
struct mount mount_list[MOUNT_MAX];

int mount_init(void)
{
	return 0;	
}

int kernel_mount(int bdev, char *name, char *type, int flags)
{
	struct fs *fs;

	if (mount_num >= MOUNT_MAX)
		return -1;

	fs = fs_get(type);

	if (!fs)
		return -1;

	if (fs->ops.mount(bdev, &mount_list[mount_num].sb) != 0)
		return -1;

	mount_list[mount_num].bdev = bdev;
	mount_list[mount_num].flags = flags;
	strncpy(mount_list[mount_num].name, name, MOUNT_NAME_MAX);
	strncpy(mount_list[mount_num].type, type, FS_NAME_MAX);
	mount_num++;
	return 0;
}

int umount(char *name)
{
	return 0;
}

struct mount *mount_get_entry(int entry)
{
	if (entry >= mount_num)
		return 0;
	if (entry < 0)
		return 0;
	return &mount_list[entry];
}

struct mount *mount_lookup(const char *pathname)
{
	int i = 0;

	for (i = 0; i < MOUNT_MAX; i++) {
		if (strneq(mount_list[i].name, pathname, MOUNT_NAME_MAX))
			return &mount_list[i];
	}

	return 0;
}
