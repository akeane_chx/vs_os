#include <lib/string.h>
#include <lib/stdio.h>
#include <lib/utmp.h>
#include <vsos/task.h>
#include <vsos/char.h>
#include <vsos/block.h>
#include <vsos/mount.h>
#include <vsos/vsfs.h>
#include <vsos/fs.h>
#include <vsos/char_dev_init.h>
#include <vsos/flash.h>
#include <vsos/board.h>
#include <vsos/sections.h>
#include <vsos/printk.h>
#include <vsos/timer.h>

#define BAUD_RATE 115200

void init_spin(void)
{
	while(1) { ; }
}

void spin_delay(int cycles)
{
        while(cycles) {
                cycles--;
        }

}

int tasklet_led(void)
{
	struct board *board = board_get();

	if(!board)
		init_spin();

	while(1) {
		board->led_on(0);
		spin_delay(10000);
		board->led_off(0);
		spin_delay(10000);
	}
}

int init(void)
{
	int root_dev = 0;
	struct board *board = board_init();
	struct task *task = 0;

	board->led_setup(0);
	board->led_on(0);
	board->led_off(0);

	board->serial_setup(BAUD_RATE);
	
	printk("\n\nVSOS 0.1 (Build:%d)\n\n", BUILD_NUMBER);
	printk("Board: %s\n", board->name);
	printk("Serial baudrate: %d\n", BAUD_RATE);
	printk("Clock rate : %d\n", board->clock_rate);
	
	fs_init();
	vsfs_init();

	block_dev_init();
	root_dev = flash_block_dev_init();

	mount_init();
	kernel_mount(root_dev, "/", "vsfs", 0);

	char_dev_init();

	null_dev_init();
	zero_dev_init();
	serial_dev_init();
	ttys_dev_init();
	sysreq_dev_init();

	task_init();
	
	task = task_get_free();	
	
	if(task) 
		task_create(task, "login", "/", login, 0); 

	task = task_get_free();	
	if(task) 
		task_create(task, "tasklet_led", "/", tasklet_led, 0); 

	board->timer_reload(board->clock_rate / timer_get_ticks_per_second());

	board->timer_start();

	init_spin();

	return 0;
}
