#include <lib/string.h>
#include <lib/stdio.h>
#include <lib/utmp.h>
#include <vsos/task.h>
#include <vsos/char.h>
#include <vsos/block.h>
#include <vsos/mount.h>
#include <vsos/vsfs.h>
#include <vsos/fs.h>
#include <vsos/flash.h>
#include <vsos/board.h>
#include <vsos/sections.h>
#include <vsos/printk.h>

#define BAUD_RATE 115200

void fault(uint32_t *sp)
{
	int i = 0;
	struct board *board  = board_get();
	struct task *task = 0;
	
	unsigned int pc = sp[6];
	unsigned int psr = sp[7];
	
	if(board)
		board->timer_stop();

	printk("\nFault:pc: 0x%x pc: 0x%x\n", pc, psr);

	for(i = 0; i < TASK_MAX; i++) {
		task = task_entry_get(i);

		if(!task) 
			break;

		pc = task->sp[14];
		psr = task->sp[15];

		switch(task->state) {
			case TASK_RUNNING:
			case TASK_NEXT:
			case TASK_READY:
			case TASK_WAITING:

			printk("Task: pc: 0x%x  psr : 0x%x  %s\n",  pc, psr, task->name);
	
			default:
				;
		}
	}
	
	while(1)
		;
}
