#include <lib/string.h>
#include <vsos/task.h>
#include <vsos/mount.h>
#include <vsos/fault.h>
#include <vsos/fs.h>
#include <vsos/syscall.h>
#include <vsos/errno.h>
#include <vsos/board.h>
#include <vsos/irq.h>

static struct task task_table[TASK_MAX];
static uint32_t *proc_stack_top;
static int task_created;

int task_init()
{
	int i = 0;
	struct board *board = board_get();

	if(!board)
		return -EINVAL;;

	proc_stack_top = ((uint32_t *) board->main_sp) - (4 * PROC_STACK_SIZE);

	for(i = 0; i < TASK_MAX; i++){
		task_table[i].state = TASK_FREE;
	}

	/* Now set the PSP to zero */
	
	asm volatile("mov r0, #0");
	asm volatile("msr psp, r0");

	return 0;
}

struct task *task_get_free()
{
	int i = 0;

	for(i = 0; i < TASK_MAX; i++) 
        	if(task_table[i].state == TASK_FREE) {
			task_table[i].pid = i;
			return &task_table[i];
	}

	return 0;
}

volatile struct task *task_current_get(void)
{
	return task_running_get();
}

struct fs_file *task_find_file_available(struct task *task)
{
	int i = 0;

	for(i = 0; i < FS_FILE_MAX; i++) {
		if(task->file[i].state == FS_FILE_AVAILABLE) {
			task->file[i].fd = i;
			return &task->file[i];
		}
	}

	return 0;
}

struct task *task_entry_get(int entry)
{
	if(entry < 0)
		return 0;
	if(entry >= TASK_MAX)
		return 0;

	return &task_table[entry];
}

int task_create(struct task *task, char *name, char *cwd, void *pc, uint32_t priority)
{
	int pid = task->pid;

	memset(task, 0, sizeof(struct task));
	task->pid = pid;

	strncpy(task->name, name, TASK_NAME_MAX);
	strncpy(task->cwd, cwd, FS_PATH_MAX);
 
	task->sp = proc_stack_top - (task->pid * PROC_STACK_SIZE);
	task->sp--;	
	/* setup initial stack frame */
	*(task->sp--) =  PSR_INIT;
	*(task->sp--) =  (unsigned int) pc & TASK_PC_MASK;
	*(task->sp--) = (unsigned int) task_exit;	/* lr */ 
	*(task->sp--) = 0; 	/* r12 */
	*(task->sp--) = 0; 	/* r3  */
	*(task->sp--) = 0; 	/* r2  */
	*(task->sp--) = 0; 	/* r1  */
	*(task->sp--) = 0; 	/* r0  */

	*(task->sp--) = 0;  	/* r11  */
	*(task->sp--) = 0; 	/* r10  */
	*(task->sp--) = 0; 	/* r9   */
	*(task->sp--) = 0; 	/* r8   */
	*(task->sp--) = 0; 	/* r7   */
	*(task->sp--) = 0; 	/* r6   */
	*(task->sp--) = 0; 	/* r5   */
	*(task->sp) = 0; 	/* r4   */

	task->priority = priority;
	task->wait = priority;	
	task->state = TASK_READY;
	task_created++;

	return 0;
}

int task_exit(void)
{
	while(1) { ; }
}

struct task *task_next_get(void)
{
	int i = 0;

	for(i = 0; i < TASK_MAX; i++) {
		if(task_table[i].state == TASK_NEXT) {
			return &(task_table[i]);
		}
	}

	return 0;
}

struct task *task_running_get(void)
{
	int i = 0;

	for(i = 0; i < TASK_MAX; i++) {
		if(task_table[i].state == TASK_RUNNING) {
			return &(task_table[i]);
		}
	}

	return (struct task *) 0;
}

int task_schedule(void)
{
	/* index of last task scheduled to be run */
	static int task_last = -1;
	int i = 0;

	/* Round robin */
	if(task_last == (task_created - 1))
		task_last = -1;

	/* find first waiting task */
	
	for(i = (task_last + 1); i < task_created; i++) {
		if(task_table[i].state == TASK_READY) { 
			task_table[i].state = TASK_NEXT;
			task_last = i;
			return 1;
		}
	}

	return 0;
	
}

void *saved_psp;

__attribute__((naked))void task_save_psp(void *sp)
{
	saved_psp = sp;
	asm volatile("bx lr");
}

void *task_get_psp(void)
{
	return saved_psp;
}

__attribute__((naked))void task_save(void *task_sp)
{
	/* Save MSP */
	asm volatile("mov r1, sp");
	/* Copy registers */
	asm volatile("mov sp, r0");
        asm volatile("push {r4-r11}");

	/* Save new PSP */
	asm volatile("mov r0, sp");
	
	/* Restore MSP */
	asm volatile("mov sp, r1");

	/* Save lr */
	asm volatile("push {lr}");

	/* Really save psp */
	asm volatile("bl task_save_psp");

	struct task *task_cur = (struct task *) 0;

	task_cur = task_running_get();

	if(task_cur) {
		task_cur->sp = task_get_psp();
	} else {
		/* We are in MSP mode already */
	} 
	
	asm volatile("pop {lr}");
	asm volatile("bx lr");	
}

__attribute__((naked)) void task_restore(void *task_sp)
{
	/* Save MSP */
	asm volatile("mov r1, sp");
	asm volatile("msr msp, r1");

	/* Restore task stackpointer */
	asm volatile("mov sp, r0");

	/* Restore sw frame */
	asm volatile("pop {r4-r11}");

	/* Restore psp */
	asm volatile("mov r0, sp");
	asm volatile("msr psp, r0");

	/* MSP back to sp */
	asm volatile("mov sp, r1");

	/* Start interrupts */
	irq_enable();
	
	/* Return from exception */
	asm volatile("mov lr, #0xfffffffd");
	asm volatile("bx lr");
}

__attribute__((naked)) void task_switch()
{
	/* Stop interrupts */
	irq_disable();

	/* Get PSP */
	asm volatile("mrs r0, psp");
	
	/* Is it non zero */
	/* then save state */
	asm volatile("cmp r0, #0");
	asm volatile("it ne");
	asm volatile("blne task_save");
		
	struct task *task_cur =  0;
	struct task *task_next = 0;
	
	task_cur = task_running_get();
	task_next = task_next_get();

	if(task_next && task_cur) {
		task_cur->state = TASK_READY;
		task_cur->wait = task_cur->priority;	
	}
	
	if(task_next) {
		task_next->state = TASK_RUNNING;
		task_restore(task_next->sp);
		/* Never return from here */
	}

	/* Start interrupts */
	irq_enable();
	
	/* Return from exception */
        asm volatile("mov lr, #0xfffffff9");
        asm volatile("bx lr");
}
