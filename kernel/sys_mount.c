#include <lib/stdio.h>
#include <vsos/task.h>
#include <vsos/syscall.h>
#include <vsos/mount.h>
#include <vsos/printk.h>

int do_mount(char *block_dev_name, char *name, char *type, int flags)
{
	int ret = 0;

	printk("syscall:mount()\n");

	/* Find the bdev !!! */

	ret = kernel_mount(0, name, type, flags);

	return ret;
}

/* Called by user task */
int sys_mount(char *block_dev_name, char *name, char *type, int flags)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(block_dev_name));
	asm volatile ("mov r1, %[d]"::[d] "r"(name));
	asm volatile ("mov r2, %[d]"::[d] "r"(type));
	asm volatile ("mov r3, %[d]"::[d] "r"(flags));
	asm volatile ("mov r4, %[d]"::[d] "r"(flags));

	asm volatile ("svc %0"::"I" (SYSCALL_MOUNT));

	return svc_ret;
}
