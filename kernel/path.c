#include <lib/string.h>
#include <lib/stdio.h>
#include <lib/fcntl.h>
#include <vsos/task.h>
#include <vsos/fs.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>
#include <vsos/char.h>
#include <vsos/errno.h>
#include <vsos/printk.h>

int path_next(const char *pathname, char *element)
{
	int i = 0;
	int e = 0;
	char c = 0;

	if (pathname[i] == 0)
		return 0;

	/* Search for slash or space */
	for (i = 0; i < FS_PATH_MAX; i++) {
		c = pathname[i];

		if (c == '/') {
			element[e] = '/';
			element[e + 1] = 0;
			return i + 1;
		}

		if (c == ' ') {
			element[e] = 0;
			return i;
		}

		if (c == 0) {
			element[e] = 0;
			return i;
		}

		element[e] = c;
		e++;
	}

	return 0;
}

int path_find_dirent(char *name, struct fs_file *file, struct fs_dirent *dirent)
{
	int err = 0;
	//printk("looking for: %s\n", name);

	if(!file->ops->open)
		return -EPERM;
	
	if(!file->ops->readdir)
		return -EPERM;

	if(!file->ops->close)
		return -EPERM;

	err = file->ops->open(file, O_RDONLY);

	if(err < 0)
		return err;

	while ((err = file->ops->readdir(file, dirent))) {
		if(err < 0) {
			file->ops->close(file);	 
			return err;
		}

		//printk("Read dir entry: %s", dirent->name);

		if (strneq(dirent->name, name, FS_NAME_MAX)) {
			/* Save the real inode device */
			int bdev = file->inode.bdev;
			//printk("found : %s\n", dirent->name);
			file->ops->close(file);

			memcpy(&file->inode, &dirent->inode,
				       		sizeof(struct inode));
			strncpy(file->name, name, FS_NAME_MAX);
			/* Restore the real inode device */
			file->inode.bdev = bdev;

			if(dirent->inode.type == FS_TYPE_DIR)
				return -EISDIR;

			return 0;
		}
	}

	printk("\n");
	file->ops->close(file);
	return -ENOENT;
}

int path_lookup(char *pathname, struct fs_file *file)
{
	int len, maxlen;
	int n = 0;
	char element[FS_NAME_MAX] = "";
	struct mount *mount = 0;
	struct fs *fs = 0;
	struct fs_dirent dirent;

	if(strnlen(pathname, FS_PATH_MAX) >= FS_PATH_MAX)
		return -ENAMETOOLONG;

	maxlen = strnlen(pathname, FS_PATH_MAX);
	len = path_next(&pathname[n], element);

	if (len  > 0) {
		/* strip off slash unless root */
		if (!strneq(element, "/", FS_PATH_MAX)) {
			if (element[len - 1] == '/')
				element[len - 1] = 0;
		}

		/* Check mount table */
		if (!mount) {
			mount = mount_lookup(element);
			if (!mount)
				return -ENOENT;
			fs = fs_get(mount->type);

			if (!fs)
				return -EPERM;

			memcpy(&file->inode, &mount->sb.i_root,
			       sizeof(struct inode));
		}

		file->inode.bdev = mount->bdev;
		file->ops = &fs->ops;
		strncpy(file->name, element, FS_NAME_MAX);
		//printk("opening: %s\n", element);

		if (len >= maxlen) {
			/* Located on root */
			return 0;
		}

		n += len;
		if(pathname[n] == '/')
			n++;

		while ((len = path_next(&pathname[n], element)) > 0) {
			int element_len = strnlen(element, FS_NAME_MAX) - 1;	
			int err = 0;
		
			/* Strip off trailing slash */	
			if(element[element_len] == '/')
				element[element_len] = 0;
			
			err = path_find_dirent(element, file, &dirent);
	
			switch(err) {
				case 0:
					return 0;
				case -EISDIR:
					if(strneq(".", file->name, FS_NAME_MAX))
						return 0;
					
					if(strneq("..", file->name, FS_NAME_MAX))
						return 0;
					break;
				default:
					return err;
			}

			n += len;
			if (n > maxlen)
				return -ENAMETOOLONG;	
		}
	}

	return 0;
}
