#include <stdint.h>
#include <vsos/bitops.h>
#include <stm32p405/timer.h>
#include <vsos/task.h>
#include <vsos/syscall.h>
#include <vsos/printk.h>
#include <vsos/fault.h>

uint32_t ticks_per_second = 100;
uint32_t task_switch_interval = 10; /* ticks before context switch */
uint32_t no_task_switch = 10;

void timer_set_ticks_per_second(uint32_t ticks)
{
	ticks_per_second = ticks;
}

uint32_t timer_get_ticks_per_second(void)
{
	return ticks_per_second;
}


void timer_set_task_switch_interval(uint32_t interval)
{
	task_switch_interval = interval;
	no_task_switch = interval;
}

uint32_t timer_get_task_switch_interval(void)
{
	return task_switch_interval;
}

void timer_func()
{
        if(no_task_switch) {
                no_task_switch--;
        } else {
	        if(task_schedule()) {
                        /* Trigger the pendSV */
                        reg_set_bits(PENDSV_BASE, PENDSV_BIT);
                	/* Reset the counter */
                	no_task_switch = task_switch_interval;
		}
        }
}
