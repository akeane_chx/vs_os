#include <lib/string.h>
#include <vsos/task.h>
#include <vsos/fs.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>
#include <vsos/errno.h>
#include <vsos/printk.h>

int do_ioctl(int fd, int cmd, void *argp)
{

	struct fs_file *file = 0;
	struct fs_ops *ops = 0;
	struct task volatile *task = 0;

	task = task_current_get();

        if(!task)
                return -EPERM;
	if(fd < 0)
                return -EBADF;

        if(fd >= FS_FILE_MAX)
                return -EBADF;

	file = (struct fs_file *) &(task->file[fd]);
	
	if(!file)
		return -EPERM;
	
	ops = task->file[fd].ops;

	if(!ops)
		return -EPERM;

	if(!ops->ioctl)
		return -EPERM;

	return ops->ioctl(file, cmd, argp);
}

int sys_ioctl(int fd, int cmd, void *argp)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(fd));
	asm volatile ("mov r1, %[d]"::[d] "r"(cmd));
	asm volatile ("mov r3, %[d]"::[d] "r"(argp));
	asm volatile ("svc %0"::"I" (SYSCALL_IOCTL));

	return svc_ret;
}
