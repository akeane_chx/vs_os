#include <lib/stdio.h>
#include <vsos/task.h>
#include <vsos/syscall.h>
#include <vsos/printk.h>

/* called by syscall handler */
int do_sleep(int delay)
{
	printk("syscall:sleep\n");
	return 0;
}

/* Called by user task */
int sys_sleep(int delay)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(delay));
	asm volatile ("svc %0"::"I" (SYSCALL_SLEEP));

	return svc_ret;
}
