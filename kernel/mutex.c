int mutex_lock(volatile unsigned char* lock)
{
  volatile unsigned char success = 1;
   
  asm( 
      "          PUSH     {R3-R4}         \n"
      "          MOV      R4, #1          \n"
      
      "          MOV      R3, #1          \n"
      "          STRB     R3, [%[Rs]]     \n"

      "          LDREXB   R3, [%[Rl]]     \n"
      "          CMP      R3, #0          \n"

      "          ITTTE    EQ              \n"
      "          STREXB   R3, R4, [%[Rl]] \n"
      "          CMP      R3, #0          \n"
      "          DMB                      \n"
      "          MOV      R3, #1          \n"
        
      "          STRB     R3, [%[Rs]]     \n"
      "          POP      {R3-R4}         \n"

      :: [Rs]"r"(&success), [Rl]"r"(lock) : "memory" );
  
  return success == 0;
}


int mutex_unlock(volatile unsigned char* lock)
{
  volatile unsigned char success = 1;

  asm (
      "          PUSH       {R3}             \n"
      "          MOV        R3, #0           \n "

      "          STRB       R3, [%[Rs]]     \n"
      "          STRB       R3, [%[Rl]]     \n"

      "          POP        {R3}              \n"

      "          DMB                        \n"
      "          DSB                        \n"

      :: [Rs]"r"(&success), [Rl]"r"(lock) : "memory" );
  
  return success == 0;
}
