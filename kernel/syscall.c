#include <stdlib.h>
#include <vsos/task.h>
#include <vsos/syscall.h>
#include <vsos/irq.h>


int syscall(void *arg)
{
	int ret = -1;
	uint32_t *args = (uint32_t *) arg;

	/*
	 * To get the SVC number we need examine the svc
	 * instruction that was called, this is at saved $pc-2
	 */

	uint8_t svc_num = 0;
	uint8_t *svc_pc = (uint8_t *) args[6];

	svc_pc -= 2;
	svc_num = *svc_pc;

	switch (svc_num) {
	case SYSCALL_SLEEP:
		ret = do_sleep((int)args[0]);
		break;
	case SYSCALL_OPEN:
		ret = do_open((char *)args[0], (int)args[1]);
		break;
	case SYSCALL_CLOSE:
		ret = do_close((int)args[0]);
		break;
	case SYSCALL_READ:
		ret = do_read((int)args[0], (char *)args[1], (int)args[2]);
		break;
	case SYSCALL_WRITE:
		ret = do_write((int)args[0], (char *)args[1], (int)args[2]);
		break;
	case SYSCALL_GETCWD:
		ret = do_getcwd((char *)args[0], (int)args[1]);
		break;
	case SYSCALL_MOUNT:
		ret = do_mount((char *)args[0], (char *) args[1], (char *)args[2], (int)args[3]);
		break;
	case SYSCALL_READDIR:
		ret = do_readdir((int)args[0], (struct fs_dirent *) args[1]);
		break;
	case SYSCALL_IOCTL:
		ret = do_ioctl((int)args[0], (int)args[1], (void *) args[2]);
	}

	return ret;
}
