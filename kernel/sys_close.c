#include <lib/string.h>
#include <errno.h>
#include <vsos/task.h>
#include <vsos/fs.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>

int do_close(int fd)
{
	struct fs_ops *ops;
	struct task volatile *task;

	if (fd >= FS_FILE_MAX)
		return -EBADF;

	if (fd < 0)
		return -EBADF;

	task = task_current_get();
	
	if(!task)
		return -EPERM;

	/* Check the fd is in use */
	if(task->file[fd].state != FS_FILE_IN_USE)
		return -EBADF;	

	/* Get the fops and call the real close */

	ops = task->file[fd].ops;
	
	if(!ops->close)
		return -EPERM;

	return ops->close((struct fs_file *) &task->file[fd]);
}

int sys_close(int fd)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(fd));
	asm volatile ("svc %0"::"I" (SYSCALL_CLOSE));

	return svc_ret;
}
