#include <lib/string.h>
#include <vsos/errno.h>
#include <lib/fcntl.h>
#include <lib/stdio.h>
#include <vsos/task.h>
#include <vsos/fs.h>
#include <vsos/char.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>
#include <vsos/path.h>
#include <vsos/printk.h>

int do_open(const char *pathname, int flags)
{
	int ret = 0;
	struct fs_file *file;
	struct char_dev *cdev;
	struct task volatile *task = 0;

	task = task_current_get();

        if(!task)
                return -EPERM;
	
	switch(flags) {
		case O_RDONLY:
		case O_WRONLY: 
		case O_RDWR:
			break;
		default:
			return -EINVAL;
	}
	
	file = task_find_file_available((struct task *) task);
	
	if(!file)
		return -EMFILE;

	ret = path_lookup(pathname, file);
	
	if(ret != 0)
		return ret;

	/* check for char device and set major minor */
	if(file->inode.type == FS_TYPE_CHAR) {

		cdev = char_dev_get(file->inode.major);

		if(!cdev)
			return -ENODEV;
		//printk("Found char device: %s\n", cdev->name);
		file->ops = &cdev->ops;
	}

	file->access_mode = flags;

	if(!file->ops)
			return -EPERM;

	if(!file->ops->open)
			return -EPERM;

	ret = file->ops->open(file, flags);

	if(ret < 0)
		return ret;

	return file->fd;
}

int sys_open(const char *pathname, int flags)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(pathname));
	asm volatile ("mov r2, %[d]"::[d] "r"(flags));
	asm volatile ("svc %0"::"I" (SYSCALL_OPEN));

	return svc_ret;
}
