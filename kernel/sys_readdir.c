#include <lib/string.h>
#include <vsos/task.h>
#include <vsos/fs.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>
#include <vsos/errno.h>

int do_readdir(int fd, struct fs_dirent *dirent)
{
	struct fs_file *file = 0;
        struct fs_ops *ops = 0;
	struct task volatile *task = 0;

        task = task_current_get();

        if(!task)
                return -EPERM;

        if(fd < 0)
                return -EBADF;

        if(fd >= FS_FILE_MAX)
                return -EBADF;

        file = (struct fs_file *) &(task->file[fd]);	

	ops = task->file[fd].ops;

        if(!ops)
                return -EPERM;

        if(!ops->readdir)
                return -EPERM;

	return ops->readdir(file, dirent);
}

int sys_readdir(int fd, struct fs_dirent *dirent)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(fd));
	asm volatile ("mov r1, %[d]"::[d] "r"(dirent));
	asm volatile ("svc %0"::"I" (SYSCALL_READDIR));

	return svc_ret;
}
