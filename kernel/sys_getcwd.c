#include <lib/string.h>
#include <lib/stdio.h>
#include <vsos/errno.h>
#include <vsos/task.h>
#include <vsos/syscall.h>
#include <vsos/printk.h>

int do_getcwd(char *buf, int size)
{
	struct task volatile *task = 0;

        task = task_current_get();

        if(!task)
                return -EPERM;
	
	strncpy(buf, (const char*) task->cwd, size);

	return 0;
}

/* Called by user task */
int sys_getcwd(char *buf, int flags)
{
	asm volatile ("mov r0, %[d]"::[d] "r"(buf));
	asm volatile ("mov r1, %[d]"::[d] "r"(flags));
	asm volatile ("svc %0"::"I" (SYSCALL_GETCWD));

	return svc_ret;
}
