#/bin/bash

# Starts openocd or qemu and start debugger

boot () {
	BOARD_BOOT="$1"
	TEST_NAME="$2"

	export BOARD=$BOARD_BOOT

	# run 'make clean'
	if ! make clean
	then
		return 1
	fi

	# run 'make to build vsos.elf'

	wget http://deployment:8080/job/VSOS/lastSuccessfulBuild/artifact/artifacts/stm32p405-vsos.elf

	if ! make debug-remote
	then
		return 1
	fi

	kill $OCD_PID
}

boot stm32p405

exit 0
