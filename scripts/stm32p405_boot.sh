#/bin/bash

# Starts openocd or qemu and start debugger

boot () {
	BOARD_BOOT="$1"
	TEST_NAME="$2"

	# Check here for openocd or qemu

	# Check openocd
	if ! ps waux | grep openocd | grep cfg
	then
		make -C openocd 2> /dev/null > /dev/null &
		OCD_PID=$!
		sleep 5
	fi

	export BOARD=$BOARD_BOOT

	# run 'make clean'
	if ! make clean
	then
		return 1
	fi

	# run 'make to build vsos.elf'
	if ! make debug
	then
		return 1
	fi

	kill $OCD_PID
}

boot stm32p405

exit 0
