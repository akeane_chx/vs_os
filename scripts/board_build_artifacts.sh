#!/bin/bash

# Script to test build of different board types

BOARDS="stm32p405 qemu-lm3s6965evb"

make clean

if ! make
then
	echo "Default board build failed"
	exit 1
fi

mkdir artifacts

for BOARD in $BOARDS
do
	make clean
	export BOARD=$BOARD
	
	if ! make
	then
		echo "$BOARD board build failed"
		exit 1
	fi

	# save the elf file

	cp vsos.elf artifacts/$BOARD-vsos.elf	
	
	make clean	
done

exit 0
