#/bin/bash

SEARCH_STR="$1"

stty -F /dev/ttyUSB0 115200

timeout --preserve-status 10 cat /dev/ttyUSB0 > serial.out &
timeout --preserve-status 10 ./scripts/stm32p405_boot-remote.sh

cat serial.out | hexdump -C

if grep $SEARCH_STR serial.out; then
	exit 0
fi

exit 1

