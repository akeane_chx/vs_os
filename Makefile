DEFAULT_BOARD := qemu-lm3s6965evb
#DEFAULT_BOARD := stm32p405

DEFAULT_BOARD_TEST := none
DEFAULT_BUILD_NUMBER := -1

# Check for BOARD in environment
ifeq ($(BOARD),)
  $(warning BOARD not set, using default)
  export BOARD := $(DEFAULT_BOARD)
endif

# Check for DEFAULT_BUILD_NUMBER in environment
ifeq ($(BUILD_NUMBER),)
  $(warning BUILD_NUMBER not set, using default)
  export BUILD_NUMBER := $(DEFAULT_BUILD_NUMBER)
endif

# Check for BOARD_TEST in environment
ifeq ($(BOARD_TEST),)
  $(warning BOARD_TEST not set, using default)
  export BOARD_TEST := $(DEFAULT_BOARD_TEST)
endif

TARGET=vsos.elf
BIN=vsos.bin
LIB := lib/libc.a
LDSCRIPT := boot/$(BOARD)/startup.ld
CROSS=arm-none-eabi-
LDFLAGS := -g -o
ROOT_OBJCOPY_FLAGS := --rename-section .data=.rootfs,contents,alloc,load,readonly,data \
							-I binary -O elf32-littlearm -B arm 

ASM_FLAGS := -g
FPU_CFLAGS := -mfloat-abi=hard -mfpu=fpv4-sp-d16
CFLAGS := -g -nostdlib -mthumb -march=armv7e-m -Werror -Wall -Os \
          -DBUILD_NUMBER=$(BUILD_NUMBER)

TTY := $(shell tty)
MACH := lm3s6965evb # Should generate this based on board type
CDEV := tty,id=any,path=$(TTY)

QEMU_DEBUG_FLAGS := -S -gdb tcp::1234 -nographic -kernel
QEMU_RUN_FLAGS := -nographic -kernel

export CFLAGS CROSS ASM_FLAGS QEMU_FLAGS MACH CDEV TARGET
export BOARD BOARD_TEST

all: objects $(TARGET) root.o

root.image:
	$(shell tools/makevsfs >> root.image)

root.o: root.image
	arm-none-eabi-objcopy $(ROOT_OBJCOPY_FLAGS) $< $@

objects: lib kernel board/$(BOARD)
	make -e -C boot/$(BOARD)
	make -C drivers/block
	make -C drivers/char
	make -C lib
	make -C minish
	make -C fs
	make -C board/$(BOARD)
	make -C kernel
	make -C tools
	make -C bin
	make -C test/tests/$(BOARD_TEST)/$(BOARD)

$(TARGET): objects root.o
	$(CROSS)ld $(LDFLAGS) $@ -T $(LDSCRIPT) $(shell cat objects) root.o
	$(RM) objects

$(BIN): $(TARGET)
	$(CROSS)objcopy -O binary $< $@
	$(RM) objects
clean:
	$(RM) *.o *.elf *.bin *.image objects
	make -C boot/$(BOARD) clean
	make -C drivers/block clean
	make -C drivers/char clean
	make -C lib clean
	make -C minish clean
	make -C kernel clean
	make -C board/$(BOARD) clean
	make -C tools clean
	make -C fs clean
	make -C bin clean
	make -C test/tests/$(BOARD_TEST)/$(BOARD) clean

debug: $(TARGET)
	make -C debug/$(BOARD) debug	

debug-remote:
	make -C debug/$(BOARD)-remote debug

test_debug: $(TARGET)
	make -C test $(BOARD_TEST)

qemu qemu-debug: $(TARGET)
	qemu-system-arm -machine $(MACH) $(QEMU_DEBUG_FLAGS) $< -chardev $(CDEV)

qemu-run: $(TARGET)
	qemu-system-arm -machine $(MACH) $(QEMU_RUN_FLAGS) $< -chardev $(CDEV)

