#include <vsos/errno.h>
#include <vsos/block.h>
#include <vsos/mount.h>
#include <vsos/syscall.h>

int read(int fd, void *buf, int count)
{
	int err = sys_read(fd, buf, count);

	if(err < 0) {
		errno = -err;
		return -1;
	}
	
	return err;
}

int write(int fd, void *buf, int count)
{
	int err = sys_write(fd, buf, count);

	if(err < 0) {
		errno = -err;
		return -1;
	}
	
	return err;
}

char *getcwd(char *buf, int size)
{
	int err = sys_getcwd(buf, size);

	if (err == 0)
		return buf;

	errno = -(err);
	return 0;
}

int close(int fd)
{
	int err = sys_close(fd);

	if(err == 0)
		return 0;
	
	errno = -(err);
	return -1;
}
