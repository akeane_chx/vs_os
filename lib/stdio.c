#include <vsos/errno.h>
#include <lib/unistd.h>
#include <lib/stdio.h>
#include <lib/string.h>
#include <vsos/board.h>

int stdin_fd = -1;
int stdout_fd = -1;
int stderr_fd = -1;

int putchar(int c)
{
	return write(stdout_fd, &c, 1);
}

int puts(const char *str)
{
	int str_count = 0;

        while (str[str_count]) {
                putchar(str[str_count]);
                str_count++;
        }

	putchar((int) '\n');
        return 0;
}

/* puts() adds a newline we don't want that for printf strings */
int puts_truncated(const char *str)
{
	int i = 0;
	while(str[i]) {
		putchar((int) str[i]);
		i++;
	}
	
	return 0;
}

int putx(int val)
{
        char str[32] = "";

        xtoa(str, val);
        return puts_truncated(str);
}

int puti(int val)
{
        char str[32] = "";

        itoa(str, val);
        return puts_truncated(str);
}

int getc(void)
{
	int err = 0;
	int c = 0;

	while((err = read(stdin_fd, &c, 1) < 0)) {
		if(errno == EAGAIN)
			continue;
		return err;
	}

	return c;
}

int gets(char *str, int size)
{
	int i = 0;

        while (i < size) {
                str[i] = (char) getc();
                if (str[i] == '\r') {
                        str[i] = 0;
                        return i;
                }
                i++;
        }

        str[i] = 0;
        return i;
}

#define ARM_FRAME_PREV_STACK_WORD_OFFSET (6)
/* Note may need to use -mapcs-frame CFLAG to enforce this */
int printf(const char *format, ...)
{
	int i = 0;
	/* Get the frame pointer */
	int *frame = __builtin_frame_address(0);
	/* first value at frame pointer is the orginal stack */ 
	int *va_args =  frame;

	va_args += ARM_FRAME_PREV_STACK_WORD_OFFSET;	
	va_args++; /* skip format arg */

	while(format[i]) {
		switch(format[i]) {
			case '%':
				i++;
				if(!format[i])
					return -1;
				switch(format[i]) {
					case '%':
						putchar((int) '%');
						va_args++;
						i++;
						break;
					case 'c':
						putchar((int) *va_args);
						va_args++;
						i++;
						break;
					case 'd':
						puti((int) *va_args);
						va_args++;
						i++;
						break;
					case 's':
						puts_truncated((char *) *va_args);
						va_args++;
						i++;
						break;
					default:
						return -1;
				}
			default:
				if(!format[i])
					return 0;			
				putchar(format[i]);
				i++;
		}
	}

	return 0;
}
