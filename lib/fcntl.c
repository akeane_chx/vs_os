#include <vsos/syscall.h>
#include <vsos/errno.h>

int open(const char *pathname, int flags)
{
	int ret = sys_open(pathname, flags);

	if(ret >= 0)
		return ret;

	errno = -(ret);
	return -1;
}
