#include <lib/stdlib.h>
#include <lib/string.h>
#include <lib/stdio.h>

/* Returns 1 if strings are equal, or zero upto n chars */
int strneq(const char *str1, const char *str2, size_t n)
{
	int i;

	for (i = 0; i < n; i++) {
		if (str1[i] == 0)
			if (str2[i] == 0)
				return 1;
		if (str1[i] != str2[i])
			return 0;
	}
	
	return 0;
}

void *memset(void *s, int c, size_t n)
{
	int i = 0;

	char *byte = (char *) s;

	for (i = 0; i < n; i++)
		byte[i] = c;

	return s;
}

void *memcpy(void *dest, void *src, size_t n)
{
	int i = 0;

	char *d = (char *) dest;
	char *s = (char *) src;

	for (i = 0; i < n; i++)
		d[i] = s[i];

	return dest;
}

/* Differs from GNU as always provides terminating NUL byte */
char *strncpy(char *d, const char *s, size_t n)
{
	int i = 0;

	memset(d, 0, n);

	for (i = 0; i < (n - 1); i++) {
		d[i] = s[i];
		if (d[i] == 0)
			return d;
	}

	return d;
}

char *strncat(char *d, const char *s, size_t n)
{
	int i = 0;
	int j = 0;
	/* Find end of dest */
	for (i = 0; i < n; i++) {
		if (d[i] == 0)
			break;
	}

	for (j = 0; j < n; j++) {
		d[i] = s[j];
		if (s[j] == 0)
			return d; 
		i++;
	}

	d[i] = 0;	
	return d;
}

char *strcat(char *d, const char *s)
{
	int i = 0;
	int j = 0;
	/* Find end of dest */
	while (d[i] == 0)
		i++;
	
	while (s[j] != 0) {
		d[i] = s[j];
		i++;
		j++;
	}

	return d;
}

int strnlen(const char *s, size_t maxlen)
{
	int i = 0;

	for (i = 0; i < maxlen; i++) {
		if (s[i] == 0)
			return i;
	}
	
	return maxlen;
}

int strlen(const char *s)
{
	int i = 0;

	while(1) {
		if (s[i] == 0)
			return i;
		i++;
	}
}

void itoa(char *str, int val)
{
	int i, rem, len = 0, n;

	if (val == 0) {
		str[0] = '0';
		str[1] = 0;
		return;
	}

	n = val;

	if(val < 0) {
		str[len] = '-';
		len++;
	}

	while (n != 0) {
		len++;
		n /= 10;
	}

	for (i = 0; i < len; i++) {
		rem = val % 10;
		val = val / 10;
		str[len - (i + 1)] = rem + '0';
	}

	str[len] = '\0';
}

void xtoa(char *str, int val)
{
	int i;
	int len = 0;
	int shift_max = 8;
	char shift_char = 0;

	for(i = shift_max; i > 0; i--) {
		shift_char = (char) ((val >> ((i - 1) * 4)) & 0xf);
		if(shift_char < 10)
			str[len] = shift_char + '0';
		else
			str[len] = shift_char - 10 + 'a';

		len++;
	} 

	str[len] = '\0';
}
