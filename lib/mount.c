#include <vsos/syscall.h>
#include <vsos/mount.h>

int mount(char *block_dev_name, char *target, char *type, int flags)
{
	int ret = 0;

	/* Remember to really get the device */

	ret = sys_mount(block_dev_name, target, type, flags);

	return ret;
}
