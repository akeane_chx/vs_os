#include <vsos/syscall.h>
#include <lib/unistd.h>
#include <lib/stdio.h>
#include <lib/fcntl.h>
#include <lib/dirent.h>
#include <vsos/errno.h>

DIR *opendir(const char *name)
{
	int fd = 0;
	static DIR dirp;

	fd = open(name, O_RDONLY);
	if (fd < 0)
		return 0;

	dirp.fd = fd;
	return &dirp;
}

int closedir(DIR *dirp)
{
	return close(dirp->fd);
}

DIR *fdopendir(int fd)
{
	return 0;
}

struct fs_dirent *readdir(DIR *dirp)
{
	int err = sys_readdir(dirp->fd, &dirp->dirent);

	/* No more entries */
	if(err == 0)
		return 0;
	
	if (err < 0) {
		errno = -err;
		return 0;
	}
	
	return &dirp->dirent;
}

int fs_print_dirent(struct fs_dirent *fs_dirent)
{
	struct inode *inode;

	inode = &(fs_dirent->inode);

	switch (inode->type) {
	case FS_TYPE_BLOCK:
		printf("brw-rw-r--");
		break;
	case FS_TYPE_CHAR:
		printf("crw-rw-r--");
		break;
	case FS_TYPE_DIR:
		printf("drwxrwxr--");
		break;
	case FS_TYPE_DIREXT:
		printf("Drwxrwxr--");
		break;
	case FS_TYPE_REG:
		printf("-rw-rw-r--");
		break;
	case FS_TYPE_REGEXT:
		printf("Erw-rw-r--");
		break;
	default:
		return -1;
	}

	printf("\t");
	printf("%s", fs_dirent->name);

	if (inode->type == FS_TYPE_CHAR)
		return 0;

	if (inode->type == FS_TYPE_BLOCK)
		return 0;

/*	printf("\t --> %d", (int) inode->block); */
	return 0;
}

int fs_print_dir(struct fs_dir *fs_dir)
{
	int i = 0;

	for (i = 0; i < fs_dir->entries; i++) {
		fs_print_dirent(&fs_dir->dirent[i]);
		printf("\n");
	}

	return 0;
}
