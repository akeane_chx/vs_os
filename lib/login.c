#include <lib/fcntl.h>
#include <lib/unistd.h>
#include <lib/sys/ioctl.h>
#include <vsos/vsfs.h>
#include <vsos/fs.h>
#include <vsos/errno.h>
#include <lib/minish.h>
#include <lib/stdio.h>
#include <lib/string.h>

#define OS_STR "vsOS"
#define OS_VER "0.1"

#if 0
#define CMD_UNAME "uname"
#define CMD_VERSION "version"
#define CMD_EXIT "exit"
#define CMD_LS "ls"
#define CMD_MOUNT "mount"
#define CMD_FPU "fpu"
#define CMD_INT "int"
#define CMD_PWD "pwd"
#endif /* 0 */

int login(void)
{
	char user_str[80] = "";
	char passwd_str[80];

	user_str[0] = 0;
	passwd_str[0] = 0;

	stdin_fd = open("/dev/ttyS0", O_RDONLY);

	if(stdin_fd < 0) 
		return -1;
	
	stdout_fd = open("/dev/ttyS0", O_WRONLY);

	if(stdout_fd < 0) 
		return -1;

	stderr_fd = open("/dev/ttyS0", O_RDONLY);

	if(stderr_fd < 0) 
		return -1;

	while(!strnlen(user_str, sizeof(user_str))) {
		printf("username: ");
		gets(user_str, sizeof(user_str));
	}

	printf("password: ");

	/* This is where echo off should be done */
	
	errno  = 0;

	ioctl(stdout_fd, 0, (void *) 0);
	printf("login : ioctl failed errno (%d)\n", errno);

	gets(passwd_str, sizeof(passwd_str));
	printf("\n");

	minish(user_str);

	close(stdin_fd);
	close(stdout_fd);
	close(stderr_fd);

	return 0;
}
