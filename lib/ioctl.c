#include <vsos/syscall.h>
#include <lib/unistd.h>
#include <lib/stdio.h>
#include <lib/fcntl.h>
#include <lib/dirent.h>
#include <vsos/errno.h>

int ioctl(int fd, int cmd, void *argp)
{
	int err = sys_ioctl(fd, cmd, argp);

	/* No more entries */
	if(err == 0)
		return 0;
	
	if (err < 0) {
		errno = -err;
		return 0;
	}
	
	return -1;
}
