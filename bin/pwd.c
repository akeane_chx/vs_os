#include <lib/unistd.h>
#include <lib/stdio.h>
#include <vsos/fs.h>

int main(int argc, char *argv[])
{
	char buf[FS_NAME_MAX] = "";

	getcwd(buf, FS_NAME_MAX);
	printf("%s \n", buf);
	return 0;
}
