#include <lib/string.h>
#include <lib/stdio.h>
#include <vsos/block.h>
#include <vsos/flash.h>
#include <vsos/printk.h>

static unsigned char *flash_addr;
static int is_open;

int flash_block_dev_open(void)
{
	flash_addr = (unsigned char *)&(_binary_root_image_start);

	printk("Opening flash\n");

	if (is_open)
		return -1;

	is_open = 1;

	return 0;
}

int flash_block_dev_release(void)
{
	is_open = 0;
	return 0;
}

int flash_block_dev_write(int block, void *data)
{
	memcpy((void *)&flash_addr[block * FLASH_BLOCK_SIZE], (void *)data,
	       FLASH_BLOCK_SIZE);
	return 0;
}

int flash_block_dev_read(int block, void *data)
{
	memcpy((void *)data, (void *)&flash_addr[block * FLASH_BLOCK_SIZE],
	       FLASH_BLOCK_SIZE);
	return 0;
}

struct block_dev flash_block_dev = {
	.name = "flash",
	.blocks = FLASH_BLOCKS,
	.block_size = FLASH_BLOCK_SIZE,
	.open = flash_block_dev_open,
	.release = flash_block_dev_release,
	.read = flash_block_dev_read,
	.write = flash_block_dev_write,
};

int flash_block_dev_init(void)
{
	return block_dev_register(&flash_block_dev);
}
