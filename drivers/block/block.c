#include <lib/string.h>
#include <lib/stdio.h>
#include <vsos/block.h>
#include <vsos/printk.h>

static int block_dev_num;
static struct block_dev *block_dev_list[BLOCK_DEV_MAX];

int block_dev_init(void)
{
	return 0;
}

int block_dev_register(struct block_dev *b)
{
	if (block_dev_num >= BLOCK_DEV_MAX)
		return -1;

	block_dev_list[block_dev_num] = b,
	
	b->id = block_dev_num;

	printk("Block device: %s : %d registered\n", b->name, b->id);

	block_dev_num++;
	return 0;
}

int block_dev_unregister(struct block_dev *b)
{
	return 0;
}

struct block_dev *block_dev_get(int bdev)
{
	if (bdev >= block_dev_num)
		return 0;
	if (bdev < 0)
		return 0;
	return block_dev_list[bdev];
}
