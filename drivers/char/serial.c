#include <vsos/board.h>
#include <lib/string.h>
#include <vsos/errno.h>
#include <vsos/char.h>
#include <vsos/printk.h>

static struct board *board;

int serial_char_dev_open(struct fs_file *file, int flags)
{
	board = board_get();
	if(!board)
		return -EPERM;
	
	file->state = FS_FILE_IN_USE;	

	return 0;
}

int serial_char_dev_close(struct fs_file *file)
{
	file->state = FS_FILE_AVAILABLE;
	return 0;
}

int serial_char_dev_read(struct fs_file *file, void *buf, int len)
{
	int err = 0;
	int i = 0;
	char *c_buf = (char *) buf;
	
	if(!board->getc)
		return -EPERM;	
	
	for(i = 0; i < len; i++) {
		if((err = board->getc()) == -EAGAIN)
			return -EAGAIN;
		
		c_buf[i] = (char) err;
		board->putchar((int) c_buf[i]);
	}

	return i;
}

int serial_char_dev_write(struct fs_file *file, void *buf, int len)
{
	int i = 0;
	char *c_buf = (char *) buf;

	if(!board->putchar)
		return -EPERM;	
	
	for(i = 0; i < len; i++)
		board->putchar(c_buf[i]); 	

	return i;
}

struct char_dev serial_char_dev = {
	.name = "serial",
	.ops.open = serial_char_dev_open,
	.ops.read = serial_char_dev_read,
	.ops.write = serial_char_dev_write,
	.ops.close = serial_char_dev_close,
	.ops.ioctl = 0,
};

int serial_dev_init(void)
{
	return char_dev_register(&serial_char_dev);
}
