#include <lib/string.h>
#include <lib/stdio.h>
#include <lib/utmp.h>
#include <vsos/task.h>
#include <vsos/char.h>
#include <vsos/block.h>
#include <vsos/mount.h>
#include <vsos/vsfs.h>
#include <vsos/fs.h>
#include <vsos/flash.h>
#include <vsos/board.h>
#include <vsos/sections.h>
#include <vsos/printk.h>
#include <vsos/errno.h>

#define BAUD_RATE 115200

int sysreq_char_dev_open(struct fs_file *file, int flags)
{
        int i = 0;
	char state_str[12] = "";
	unsigned int pc = 0;
	unsigned int psr = 0;
        struct task *task = 0;

	struct board *board  = board_get();

	if(!board)
		return -ENODEV;

	printk("Board Name     : %s\n", board->name);
	printk("bss_start      : %x\n", board->bss_start);
	printk("bss_end        : %x\n", board->bss_end);
	printk("data_vma       : %x\n", board->data_vma);
	printk("data_lma       : %x\n", board->data_lma);
	printk("data_end       : %x\n", board->data_end);
	printk("flash_fs_start : %x\n", board->flash_fs_start);
	printk("flash_fs_end   : %x\n", board->flash_fs_end);
	printk("main_sp        : %x\n", board->main_sp);
	printk("bss_size       : %x\n", board->bss_size);
	printk("data_size      : %x\n", board->data_size);

	for(i = 0; i < TASK_MAX; i++) {
		task = task_entry_get(i);

		if(!task)
			break;

		pc = task->sp[14];
		psr = task->sp[15];

		switch(task->state) {
			case TASK_RUNNING:
				strncpy(&state_str[0], "Running ", sizeof(state_str));
				break;
			case TASK_NEXT:
				strncpy(&state_str[0], "Next    ", sizeof(state_str));
				break;
			case TASK_READY:
				strncpy(&state_str[0], "Ready   ", sizeof(state_str));
				break;
			case TASK_WAITING:
				strncpy(&state_str[0], "Waiting ", sizeof(state_str));
				break;
			default:
				strncpy(&state_str[0], "Unknown ", sizeof(state_str));
		}
		printk("Task: pc: 0x%x  psr : 0x%x  state : %s %s\n",  pc, psr, state_str, task->name);
	}

	return 0;
}

int sysreq_char_dev_close(struct fs_file *file)
{
	return 0;
}

int sysreq_char_dev_read(struct fs_file *file, void *buf, int len)
{
	return 0;
}

int sysreq_char_dev_write(struct fs_file *file, void *buf, int len)
{
	return -EPERM;
}

struct char_dev sysreq_char_dev = {
	.name = "sysreq",
	.ops.open = sysreq_char_dev_open,
	.ops.read = sysreq_char_dev_read,
	.ops.write = sysreq_char_dev_write,
	.ops.close = sysreq_char_dev_close,
	.ops.ioctl = 0,
};

int sysreq_dev_init(void)
{
	return char_dev_register(&sysreq_char_dev);
}
