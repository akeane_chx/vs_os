#include <lib/string.h>
#include <vsos/errno.h>
#include <vsos/char.h>

int null_char_dev_open(struct fs_file *file, int flags)
{
	return 0;
}

int null_char_dev_close(struct fs_file *file)
{
	return 0;
}

int null_char_dev_read(struct fs_file *file, void *buf, int len)
{
	return 0;
}

int null_char_dev_write(struct fs_file *file, void *buf, int len)
{
	return -EPERM;
}

struct char_dev null_char_dev = {
	.name = "null",
	.ops.open = null_char_dev_open,
	.ops.read = null_char_dev_read,
	.ops.write = null_char_dev_write,
	.ops.close = null_char_dev_close,
	.ops.ioctl = 0,
};

int null_dev_init(void)
{
	return char_dev_register(&null_char_dev);
}
