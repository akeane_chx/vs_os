#include <lib/string.h>
#include <vsos/errno.h>
#include <vsos/char.h>

int zero_char_dev_open(struct fs_file *file, int flags)
{
	return -EPERM;
}

int zero_char_dev_close(struct fs_file *file)
{
	return -EPERM;
}

int zero_char_dev_read(struct fs_file *file, void *buf, int len)
{
	return -EPERM;
}

int zero_char_dev_write(struct fs_file *file, void *buf, int len)
{
	return -EPERM;
}

struct char_dev zero_char_dev = {
        .name = "zero",
        .ops.open = zero_char_dev_open,
        .ops.read = zero_char_dev_read,
        .ops.write = zero_char_dev_write,
        .ops.close = zero_char_dev_close,
        .ops.ioctl = 0,
};

int zero_dev_init(void)
{
        return char_dev_register(&zero_char_dev);
}
