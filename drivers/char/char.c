#include <lib/stdio.h>
#include <lib/string.h>
#include <vsos/char.h>
#include <vsos/printk.h>

static int char_dev_num;
static struct char_dev *char_dev_list[CHAR_DEV_MAX];

int char_dev_init(void)
{
	return 0;
}

int char_dev_register(struct char_dev *c)
{
	if (char_dev_num >= CHAR_DEV_MAX)
		return -1;

	char_dev_list[char_dev_num] = c;
	c->major = char_dev_num;

	printk("Character device: \'%s\' : %d  registered\n", c->name, c->major);	
	char_dev_num++;
	return c->major;
}

int char_dev_unregister(struct char_dev *b)
{
	return 0;
}

struct char_dev *char_dev_get(int cdev)
{
	if (cdev >= char_dev_num)
		return 0;
	if (cdev < 0)
		return 0;

	return char_dev_list[cdev];
}
