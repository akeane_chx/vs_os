#ifndef __LED_H
#define __LED_H

#include <stdint.h>

int _led_setup(int mask);
int _led_on(int led);
int _led_off(int led);

#endif /* __LED_H */
