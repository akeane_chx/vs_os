#ifndef __SERIAL_H
#define __SERIAL_H
#include <stdint.h>

#define UART0_DR	((uint32_t *) 0x4000C000)
#define UART0_RSR	((uint32_t *) 0x4000C004)
#define UART0_FR	((uint32_t *) 0x4000C018)
#define UART0_CTL	((uint32_t *) 0x4000C030)

#define UART_FR_RXFF	(1 << 6)	/* TX FIFO full */
#define UART_FR_TXFF	(1 << 5)	/* TX FIFO full */

#define UART_FR_TXFE	(1 << 7)	/* TX FIFO empty */
#define UART_FR_RXFE	(1 << 4)	/* RX FIFO empty */
#define UART_FR_BUSY	(1 << 3)	/* TX is busy */

#include <stdint.h>
#include <qemu-lm3s6965evb/serial.h>
#include <vsos/bitops.h>

int _serial_setup(int baud_rate);
int _serial_putchar(int c);
void _serial_puts(unsigned char *str);
int _serial_getc(void);
int _serial_gets(char *str, int size, int echo);
#endif				/* __SERIAL_H */
