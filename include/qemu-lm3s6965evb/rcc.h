#ifndef __RCC_H
#define __RCC_H

#include <stdint.h>
#define MHZ ((uint32_t) 1000000)

#define RCC_HSI_MHZ 12 * MHZ /* Internal clock rate */

#define RCC_CR		((uint32_t *)0x40023800)
#define RCC_PLLCFGR	((uint32_t *)0x40023804)
#define RCC_CFGR	((uint32_t *)0x40023808)
#define RCC_CIR		((uint32_t *)0x4002380c)
#define RCC_AHB1RSTR	((uint32_t *)0x40023810)
#define RCC_AHB2RSTR	((uint32_t *)0x40023814)
#define RCC_AHB3RSTR	((uint32_t *)0x40023818)

#define RCC_APB1RSTR	((uint32_t *)0x40023820)
#define RCC_APB2RSTR	((uint32_t *)0x40023824)

#define RCC_AHB1ENR	((uint32_t *)0x40023830)
#define RCC_AHB2ENR	((uint32_t *)0x40023834)
#define RCC_AHB3ENR	((uint32_t *)0x40023838)

#define RCC_APB1ENR	((uint32_t *)0x40023840)
#define RCC_APB2ENR	((uint32_t *)0x40023844)

#define RCC_CR_PLLON (1 << 24)
#define RCC_CR_PLLRDY (1 << 25)

#define RCC_CR_HSEON (1 << 16)
#define RCC_CR_HSERDY (1 << 17)

#define RCC_CFGR_SW_PLL (0x2)

#define RCC_GPIO_A_RST (1 << 0)
#define RCC_GPIO_B_RST (1 << 1)
#define RCC_GPIO_C_RST (1 << 2)
#define RCC_GPIO_D_RST (1 << 3)
#define RCC_GPIO_E_RST (1 << 4)
#define RCC_GPIO_F_RST (1 << 5)

#define RCC_GPIO_A_EN (1 << 0)
#define RCC_GPIO_B_EN (1 << 1)
#define RCC_GPIO_C_EN (1 << 2)
#define RCC_GPIO_D_EN (1 << 3)
#define RCC_GPIO_E_EN (1 << 4)
#define RCC_GPIO_F_EN (1 << 5)

#define RCC_USART2_EN (1 << 17)

#endif /* _RCC_H */
