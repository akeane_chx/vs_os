#ifndef __TIMER_H
#define __TIMER_H

#define SYSTICK_BASE ((volatile uint32_t *) 0xe000e000)
#define SYSTICK_CTRL ((volatile uint32_t *) 0xe000e010)
#define SYSTICK_TRELOAD ((volatile uint32_t *) 0xe000e014)
#define SYSTICK_CTRL_ACTIVATE (0x7)
#define PENDSV_BASE ((volatile uint32_t *) 0xE000ED04)
#define PENDSV_BIT ((uint32_t) 0x10000000)

void _timer_start();
void _timer_stop();
void _timer_reload(unsigned reload);
__attribute__((interrupt)) void timer_func();
#endif /* __TIMER_H */
