#ifndef __SYSCALL_H
#define __SYSCALL_H

#include <stdint.h>
#include <vsos/fs.h>

#define SYSCALL_OPEN  1
#define SYSCALL_CLOSE 2
#define SYSCALL_READ  3
#define SYSCALL_WRITE 4
#define SYSCALL_SLEEP 5
#define SYSCALL_GETCWD 6
#define SYSCALL_MOUNT 7
#define SYSCALL_READDIR 8
#define SYSCALL_IOCTL 9

extern int svc_in;
extern int svc_ret;

int syscall(void *arg);

/* Kernel funcs */
int do_getcwd(char *buf, int flags);
int do_mount(char *block_dev_name, char *name, char *type, int flags);
int do_open(const char *pathname, int flags);
int do_read(int fd, void *buf, int len);
int do_write(int fd, void *buf, int len);
int do_close(int fd);
int do_readdir(int fd, struct fs_dirent *dirent);
int do_sleep(int s);
int do_ioctl(int fd, int cmd, void *argp);

/* Called from user task, setup regs and issue SVC instruction */
int sys_getcwd(char *buf, int flags);
int sys_mount(char *block_dev_name, char *name, char *type, int flags);
int sys_open(const char *pathname, int flags);
int sys_read(int fd, void *buf, int len);
int sys_write(int fd, void *buf, int len);
int sys_close(int fd);
int sys_readdir(int fd, struct fs_dirent *dirent);
int sys_sleep(int s);
int sys_ioctl(int fd, int cmd, void *argp);
#endif /* __SYSCALL_H */
