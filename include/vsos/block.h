#ifndef __BLOCK_H
#define __BLOCK_H

#include <stdint.h>

#define BLOCK_DEV_MAX 4
#define BLOCK_DEV_NAME_MAX 16

struct block_dev {
	char name[BLOCK_DEV_NAME_MAX];
	int id;
	int blocks;
	int block_size;
	int (*open) (void);
	int (*release) (void);

	int (*read) (int block, void *data);
	int (*write) (int block, void *data); 
};

int block_dev_init();
int block_dev_register(struct block_dev *b);
int block_dev_unregister(struct block_dev *b);
int block_dev_open(int id);
int block_dev_release(int id);
int block_dev_read(int id, int block, void *data);
int block_dev_write(int id, int block, void *data);
int block_dev_blocks(int id);
int block_dev_block_size(int id);
struct block_dev *block_dev_get(int bdev);
#endif /* __VSFS_H */
