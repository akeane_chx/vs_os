#ifndef __MOUNT_H
#define __MOUNT_H

#include <stdint.h>
#include <vsos/fs.h>

#define MOUNT_MAX 4
#define MOUNT_NAME_MAX 16

struct mount {
	int bdev;
	char name[MOUNT_NAME_MAX];
	char type[MOUNT_NAME_MAX];
	int flags;
	struct superblock sb;
};

int kernel_mount(int bdev, char *name, char *type, int flags);
int mount_init();
int kernel_mount(int bdev, char *name, char *type, int flags);
int umount(char *name);
struct mount *mount_get_entry(int entry);
struct mount *mount_lookup(const char *pathname);
#endif /* __VSFS_H */
