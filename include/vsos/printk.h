#ifndef __PRINTK_H
#define __PRINTK_H
int printk(const char *format, ...);

#endif /* __PRINTK_H */
