#ifndef __SECTIONS_H
#define __SECTIONS_H
extern void *__main_sp;
extern void *__bss_start;
extern void *__bss_end;
extern void *__data_vma;
extern void *__data_lma;
extern void *__data_end;
extern void *__flash_fs_start;
extern void *__flash_fs_end;
extern void *__flash_fs_block_size;
#endif /* __SECTIONS_H */
