#ifndef __CHAR_DEV_INIT_H
#define __CHAR_DEV_INIT_H

/* Should come up with a way to register the devices as part of build */

int null_dev_init(void);
int serial_dev_init(void);
int ttys_dev_init(void);
int zero_dev_init(void);
int sysreq_dev_init(void);

#endif /* __CHAR_DEV_INIT_H */
