#ifndef __BOARD_H
#define __BOARD_H

#define BOARD_NAME_MAX 16

struct board {
	void *bss_start;
	void *bss_end;
	void *data_vma;
	void *data_lma;
	void *data_end;
	void *flash_fs_start;
	void *flash_fs_end;
	void *main_sp; /* Main stack pointer */
	unsigned int bss_size;
	unsigned int data_size;
	unsigned int clock_hsi; /* Internal clock */
	unsigned int clock_hse; /* External clock */
	unsigned int clock_rate; /* Current clock speed */
	char name[BOARD_NAME_MAX];
	int (*serial_setup)(int baudrate);
	int (*putchar)(int c);
	int (*puts)(const char *str);
	int (*getc)(void);
	int (*gets)(char *str, int size, int echo);
	int (*led_setup)(int led_mask);
	int (*led_on)(int led);
	int (*led_off)(int led);
	void (*timer_start)(void);
	void (*timer_stop)(void);
	void (*timer_reload)(unsigned reload);
};

struct board *board_init(void);
struct board *board_get(void);

#endif /* __BOARD_H */
