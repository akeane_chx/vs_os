#ifndef __CHAR_H
#define __CHAR_H

#include <stdint.h>
#include <vsos/fs.h>

#define CHAR_DEV_MAX 16
#define CHAR_DEV_NAME_MAX 16

struct char_dev {
	char name[CHAR_DEV_NAME_MAX];
	int major;
	struct fs_ops ops;
};

int char_dev_init();
int char_dev_register(struct char_dev *c);
int char_dev_unregister(struct char_dev *c);
struct char_dev *char_dev_get(int cdev);
#endif /* __CHAR_H */
