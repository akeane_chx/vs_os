#ifndef __BITOPS_H
#define __BITOPS_H

#include <stdint.h>

/* Write value to register address */
static inline
volatile uint32_t reg_read(volatile uint32_t *reg_ptr)
{
        return *reg_ptr;
}

/* Write value to register address */
static inline
void reg_set(volatile uint32_t *reg_ptr, uint32_t val)
{
        *reg_ptr = val;
}

/* Copy register OR bits in and write back */
static inline
void reg_set_bits(volatile uint32_t *reg_ptr, uint32_t val)
{
        uint32_t reg;
        reg = *reg_ptr;
        reg |= val;
        *reg_ptr = reg;
}

/* Copy register OR bits in and write back */
static inline
void reg_unset_bits(volatile uint32_t *reg_ptr, uint32_t val)
{
        uint32_t reg;
        reg = *reg_ptr;
        reg &= ~val;
        *reg_ptr = reg;
}

/* Copy register unset mask bits set bits*/
static inline
void reg_set_masked_bits(volatile uint32_t *reg_ptr, uint32_t val, uint32_t mask)
{
	reg_unset_bits(reg_ptr, mask);
	reg_set_bits(reg_ptr, mask);
}


#endif /* _BITOPS_H */
