#ifndef __IRQ_H
#define __IRQ_H

/* Maybe should wrap in do { .. } while(0) */
/* But would that mean extra instructions precede the asm? */

#define irq_disable() \
			asm volatile ("cpsid i");\
			asm volatile ("dsb");\
			asm volatile ("isb");

#define irq_enable() asm volatile ("cpsie i");

#endif /* __IRQ_H */
