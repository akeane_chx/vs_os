#ifndef __PATH_H
#define __PATH_H
int path_lookup(const char *pathname, struct fs_file *file);
#endif /* __PATH_H */
