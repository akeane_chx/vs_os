#ifndef __ERRNO_H
#define __ERRNO_H

extern int errno;

#define EPERM	1 /* Permisson error */
#define ENOENT	2 /* No such file or dir */
#define EBADF	9 /* Bad file descriptor */
#define EAGAIN  11      /* Try again */
#define EBUSY	16 /* Device busy */

#define ENODEV	19 /* No such device */
#define ENOTDIR	20 /* Not a directory */
#define EISDIR	21 /* Is a directory */
#define EINVAL	22 /* Invalid argument */
#define EMFILE	24 /* Too many files open */

#define ENAMETOOLONG	36 /* file or pathname too long */

#if 0
#undef EACCES          //Permission denied (POSIX.1)
#undef EADDRINUSE      //Address already in use (POSIX.1)
#undef EADDRNOTAVAIL   //Address not available (POSIX.1)
#undef EAFNOSUPPORT    //Address family not supported (POSIX.1)
#undef EAGAIN          //Resource temporarily unavailable (may be the same value as EWOULDBLOCK) (POSIX.1)
#undef EALREADY        //Connection already in progress (POSIX.1#undef)
#undef EBADE           //Invalid #undefexchange
#undef EBADF           //Bad file descriptor (POSIX.1)
#undef EBADFD          //File descriptor in bad state
#undef EBADMSG         //Bad message (POSIX.1)
#undef EBADR           //Invalid request descriptor
#undef EBADRQC         //Invalid request code
#undef EBADSLT         //Invalid slot
#undef ECANCELED       //Operation canceled (POSIX.1)
#undef ECHILD          //No child processes (POSIX.1)
#undef ECHRNG          //Channel number out of range
#undef ECOMM           //Communication error on send
#undef ECONNABORTED    //Connection aborted (POSIX.1#undef)
#undef ECONNREFUSED    //Connection refused (POSIX.1)
#undef ECONNRESET      //Connection reset (POSIX.1)
#undef EDEADLK         //Resource deadlock avoided (POSIX.1)
#undef EDEADLOCK       //Synonym #define for EDEADLK
#undef EDESTADDRREQ    //Destination address required (POSIX.1)
#undef EDOM            //Mathematics argument out of domain of function (POSIX.1, C99)
#undef EDQUOT          //Disk quota exceeded (POSIX.1)
#undef EEXIST          //File exists (POSIX.1#define )
#undef EFAULT          //Bad address (POSIX.1)
#undef EFBIG           //File too large (POSIX.#undef1)
#undef EHOSTDOWN       //Host is down
#undef EHOSTUNREACH    //Host is unreachable (POSIX.1)
#undef EILSEQ          //Illegal byte sequence (POSIX.1, C99)
#undef EINPROGRESS     //Operation in progress (POSIX.1)
#undef EINTR           //Interrupted function call (POSIX.1); see signal(7).
#undef EIO             //Input/output error (POSIX.1)
#undef EISCONN         //Socket is connected (POSIX.1)
#undef EISDIR          //Is a directory (POSIX.1)
#undef EISNAM          //Is a named type file
#undef EKEYEXPIRED    // Key has expired
#undef EKEYREJECTED    //Key was rejected by service
#undef EKEYREVOKED     //Key has been revoked
#undef EL2HLT          //Level 2 halted
#undef EL2NSYNC        //Level 2 #define not synchronized
#undef EL3HLT          //Level 3 halted
#undef EL3RST          //Level 3 #undefhalted
#undef ELIBACC         //Cannot access a needed shared library
#undef ELIBBAD         //Accessing a corrupted shared #define #undeflibrary
#undef ELIBMAX         //Attempting to link in too many shared libraries
#undef ELIBSCN         //lib section in a.out corrupted
#undef ELIBEXEC        //Cannot exec a shared library directly
#undef ELOOP           //Too many levels of symbolic links (POSIX.1)
#undef EMEDIUMTYPE     //Wrong medium type
#undef EMFILE          //Too  many  open  files  (POSIX.1);  commonly caused by exceeding the RLIMIT_NOFILE resource limit
#undef EMSGSIZE        //Message too long (POSIX.1)
#undef EMULTIHOP       //Multihop attempted (POSIX.1)
#undef ENAMETOOLONG    //Filename too long (POSIX.1)
#undef ENETDOWN        //Network is down (POSIX.1)
#undef ENETRESET       //Connection aborted by network (POSIX.1#undef)
#undef ENETUNREACH     //Network unreachable (POSIX.1)
#undef ENFILE          //Too many open files in system (POSIX.1); on Linux, this is probably a result of encountering  the
#undef ENOBUFS         //No buffer space available (POSIX.1 (XSI STREAMS option))
#undef ENODATA         //No message is available on the STREAM head read queue (POSIX.1)
#undef ENODEV          //No such device (POSIX.1#undef)
#undef ENOENT          //No such file or directory (POSIX.1)
#undef ENOEXEC         //Exec format error (POSIX.1#define )
#undef ENOKEY          //Required key not available
#undef ENOLCK          //No locks available (POSIX.1)
#undef ENOLINK         //Link has been severed (POSIX.1)
#undef ENOMEDIUM       //No medium found
#undef ENOMEM          //Not enough space (POSIX.1)
#undef ENOMSG          //No message of the desired type (POSIX.1)
#undef ENONET          //Machine is not on the network
#undef ENOPKG          //Package not installed
#undef ENOPROTOOPT     //Protocol not available (POSIX.1)
#undef ENOSPC          //No space left on device (POSIX.1)
#undef ENOSR           //No STREAM resources (POSIX.1 (XSI STREAMS option))
#undef ENOSTR         // Not a STREAM (POSIX.1 (XSI STREAMS option))
#undef ENOSYS          //Function not implemented (POSIX.1)
#undef ENOTBLK         //Block device required
#undef ENOTCONN        //The socket is not connected (POSIX.#undef1)
#undef ENOTDIR         //Not a directory (POSIX.1)
#undef ENOTEMPTY       //Directory not empty (POSIX.1)
#undef ENOTSOCK        //Not a socket (POSIX.1)
#undef ENOTSUP         //Operation not supported (POSIX.1)
#undef ENOTUNIQ        //Name not unique on #define network
#undef ENXIO           //No such device or address (POSIX.1)
#undef EOPNOTSUPP      //Operation not supported on socket (POSIX.1)
#undef EOVERFLOW       //Value too large to be stored in data type (POSIX.1)
#undef EPERM           //Operation not permitted (POSIX#undef.1)
#undef EPFNOSUPPORT    //Protocol family not supported
#undef EPIPE           //Broken pipe (POSIX.1)
#undef EPROTO          //Protocol error (POSIX.1)
#undef EPROTONOSUPPORT //Protocol not supported (POSIX.1)
#undef EPROTOTYPE      //Protocol wrong type for socket (POSIX.1#define )
#undef EREMCHG         //Remote address changed
#undef EREMOTE         //Object is remote
#undef EREMOTEIO       //Remote I/O error
#undef ERESTART        //Interrupted system call should be restarted
#undef EROFS           //Read-only filesystem (POSIX.1#define )
#undef ESHUTDOWN       //Cannot send after transport endpoint shutdown
#undef ESPIPE          //Invalid seek (POSIX.1)
#undef ESOCKTNOSUPPORT //Socket type not supported
#undef ESRCH           //No such process (POSIX.1)
#undef ESTALE          //Stale file handle (POSIX.1)
#undef ESTRPIPE        //Streams pipe error
#undef ETIME           //Timer expired (POSIX.1 (XSI STREAMS option))
#undef ETIMEDOUT       //Connection timed out (POSIX.1)
#undef ETXTBSY         //Text file busy (POSIX.1)
#undef EUCLEAN         //Structure needs cleaning
#undef EUNATCH         //Protocol driver not attached
#undef EUSERS          //Too many users
#undef EWOULDBLOCK     //Operation would block (may be same value as EAGAIN) (POSIX.1)
#undef EXDEV           //Improper link (POSIX.1)
#undef EXFULL          //Exchange full
#endif /* 0 */
#endif /* ERRNO_H */
