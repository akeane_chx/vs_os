#ifndef __TIMER_H
#define __TIMER_H

#include <stdint.h>

void timer_set_ticks_per_second(uint32_t ticks);
uint32_t timer_get_ticks_per_second(void);
void timer_set_task_switch_interval(uint32_t interval);
uint32_t timer_get_task_switch_interval(void);
void timer_func();

#endif /* __TIMER_H */
