#ifndef __FLASH_H
#define __FLASH_H

/* Should really derive these from board info */
#define FLASH_BLOCKS 512
#define FLASH_BLOCK_SIZE 512

extern void *_binary_root_image_start;
#include <lib/string.h>
#include <lib/stdio.h>
#include <vsos/block.h>
#include <vsos/flash.h>

int flash_block_dev_open(void);
int flash_block_dev_release(void);
int flash_block_dev_write(int block, void *data);
int flash_block_dev_read(int block, void *data);
int flash_block_dev_init(void);

#endif /* __FLASH_H */
