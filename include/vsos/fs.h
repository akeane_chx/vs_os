#ifndef __FS_H
#define __FS_H

#include <stdint.h>

#define FS_TYPE_BLOCK		(0x1)
#define FS_TYPE_CHAR		(0x2)
#define FS_TYPE_DIR		(0x3)
#define FS_TYPE_DIREXT		(0x4)
#define FS_TYPE_REG		(0x5)
#define FS_TYPE_REGEXT		(0x6)
#define FS_TYPE_LINK		(0x7)
#define FS_TYPE_MOUNT		(0x8)

#define FS_MAX 4
#define FS_NAME_MAX 16
#define FS_DIRENT_MAX 16

#define FS_FILE_MAX 8 /* max open files */

#define FS_PATH_MAX 256

#define FS_FILE_AVAILABLE	0 /* unused file descriptor */
#define FS_FILE_IN_USE		1 /* file descriptor in use */

struct inode {
	uint8_t bdev;
	uint8_t type;
	uint8_t major;
	uint8_t minor;
	uint16_t len;
	uint16_t perm;
	uint32_t block;
};

struct superblock {
	int id;
	int blocks;
	int block_size;
	int dirty;
	struct inode i_root;
};

struct fs_dirent {
        char name[FS_NAME_MAX];
        struct inode inode;
};

struct fs_dir {
        uint8_t entries;
        struct fs_dirent dirent[FS_DIRENT_MAX];
};

struct fs_file {
	int fd;
	struct inode inode;
	char name[FS_NAME_MAX];
	int pos;
	int state;
	int flags;
	int dentries;
	int access_mode;
	struct fs_ops *ops;
};

struct fs_ops {
	int (*mount) (int bdev, struct superblock *sb);
	int (*open) (struct fs_file *file, int flags);
	int (*close) (struct fs_file *file);
	int (*readdir) (struct fs_file *file, struct fs_dirent *dirent);
	int (*read) (struct fs_file *, void *buf, int len);
	int (*write) (struct fs_file *, void *buf, int len);
	int (*ioctl) (struct fs_file *, int cmd, void *argp);
};

struct fs {
	int id;
	char name[FS_NAME_MAX];
	struct fs_ops ops;
};

int fs_init();
struct fs *fs_get(char *name);
int fs_register(struct fs *f);
int fs_unregister(struct fs *f);
int fs_print_dirent(struct fs_dirent *fs_dirent);
int fs_print_dir(struct fs_dir *fs_dir);
int fs_opendir(char *pathname);
#endif /* __FS_H */
