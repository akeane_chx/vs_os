#ifndef __TASK_H
#define __TASK_H

#include <stdint.h>
#include <vsos/fs.h>

#define TASK_MAX 4

#define TASK_FREE 	0	
#define TASK_WAITING	1	
#define TASK_READY 	2
#define TASK_RUNNING 	3
#define TASK_NEXT	4
#define TASK_EXIT	5

#define TASK_NAME_MAX 16

/* Need to mask out thumb bit */
#define TASK_PC_MASK 0xfffffffe

#define PROC_STACK_SIZE 1024

#define PSR_INIT 0x21000000 

#define EXC_RET_THREAD 0xfffffff9
#define EXC_RET_KERNEL 0xfffffffd

struct task {
	int pid;
	char name[TASK_NAME_MAX];
	char cwd[FS_NAME_MAX];
	struct inode cwd_inode;
	struct fs_file file[FS_FILE_MAX];
	int files_open;
	
	/* Stack pointer */
	uint32_t *sp;
	uint32_t state;
	
	/* Lower value is higher priority */
	int32_t priority;

	/* Each time the timer_func is called this gets
	   decremented, if it reached 0 then the task is 
	   set to TASK_READY and the task_wait is reinitialised
	   with priority */

	int32_t wait;
};

int task_init();
struct task *task_entry_get(int entry);
struct task *task_get_free();
int task_create(struct task *task, char *name, char *cwd, void *pc, uint32_t priority);
struct task *task_ready();
__attribute__((naked)) void task_switch(void);
int task_schedule(void);
int task_exit(void);

struct task *task_running_get(void);
volatile struct task *task_current_get(void);
struct fs_file *task_find_file_available(struct task *task);
#endif /* __TASK_H */
