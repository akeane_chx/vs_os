#ifndef __VSFS_H
#define __VSFS_H

#include <stdint.h>
#include <vsos/fs.h>

#define VSFS_ID	0x3342

#define VSFS_TYPE_BLOCK		(0x1)
#define VSFS_TYPE_CHAR		(0x2)
#define VSFS_TYPE_DIR		(0x3)
#define VSFS_TYPE_DIREXT	(0x4)
#define VSFS_TYPE_REG		(0x5)
#define VSFS_TYPE_REGEXT	(0x6)

#define VSFS_BLOCK_SIZE 512
#define VSFS_NAME_MAX  16	
#define VSFS_DIRENT_MAX 16
#define VSFS_MAX_BLOCKS 512

struct vsfs_inode {
	uint8_t type;
	uint8_t major;
	uint8_t minor;
	uint16_t len;
	uint16_t perm;
	uint32_t block;
} __attribute__((packed));

struct vsfs_dirent {
	char name[VSFS_NAME_MAX];
	struct vsfs_inode inode;
} __attribute__((packed));

struct vsfs_dir {
	uint8_t entries;
	struct vsfs_dirent dirent[VSFS_DIRENT_MAX];
};

struct vsfs_superblock {
	uint16_t id;
	uint16_t blocks;
	uint16_t block_size;
	int dirty;
	struct vsfs_inode i_root;
} __attribute__((packed));

int vsfs_dirent_to_fs(struct fs_dirent *fs_dirent, struct vsfs_dirent *dirent);
int vsfs_mount(int bdev, struct superblock *sb);
int vsfs_open(struct fs_file *file, int flags);
int vsfs_read(struct fs_file *file, void *buf, int len);
int vsfs_readdir(struct fs_file *file, struct fs_dirent *dirent);
int vsfs_close(struct fs_file *file);
int vsfs_init(void);

#endif /* __VSFS_H */
