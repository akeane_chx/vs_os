#ifndef __FAULT_H
#define __FAULTK_H

#include <stdint.h>

void fault(uint32_t *sp);

#endif /* __FAULT_H */
