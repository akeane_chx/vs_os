#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdint.h>

#define USART2_SR	((uint32_t *)0x40004400)
#define USART2_DR	((uint32_t *)0x40004404)
#define USART2_BRR	((uint32_t *)0x40004408)
#define USART2_CR1	((uint32_t *)0x4000440c)
#define USART2_CR2	((uint32_t *)0x40004410)
#define USART2_CR3	((uint32_t *)0x40004414)

#define USART_RXNE	(1 << 5) /* RX Data can be read */
#define USART_TXE	(1 << 7) /* TX Data not transferred */

#define USART_RX_EN	(1 << 2)
#define USART_TX_EN	(1 << 3)
#define USART_EN	(1 << 13)

#define GPIO_PIN_TX 2
#define GPIO_PIN_RX 3

int _serial_setup(int baudrate);
int _serial_putchar(int c);
int _serial_puts(char *str);
int _serial_getc(void);
int _serial_gets(char *str, int size, int echo);
#endif /* __SERIAL_H */
