#ifndef __FPU_H
#define __FPU_H

#include <stdint.h>

#define FPU_CPACR	((uint32_t *)0xE000ED88)

#define FPU_EN (0xf << 20)

#endif /* _FPU_H */
