#ifndef __GPIO_H
#define __GPIO_H
#include <stdint.h>

#define GPIOA_MODER ((uint32_t *) 0x40020000)
#define GPIOB_MODER ((uint32_t *) 0x40020400)
#define GPIOC_MODER ((uint32_t *) 0x40020800)
#define GPIOD_MODER ((uint32_t *) 0x40020c00)
#define GPIOE_MODER ((uint32_t *) 0x40021000)
#define GPIOF_MODER ((uint32_t *) 0x40021400)
#define GPIOG_MODER ((uint32_t *) 0x40021800)
#define GPIOH_MODER ((uint32_t *) 0x40021c00)
#define GPIOI_MODER ((uint32_t *) 0x40022000)
#define GPIOJ_MODER ((uint32_t *) 0x40022400)
#define GPIOK_MODER ((uint32_t *) 0x40022800)

#define GPIO_MODE_INPUT		(0x0)
#define GPIO_MODE_GPO		(0x1)
#define GPIO_MODE_AF		(0x2)
#define GPIO_MODE_ANALOG	(0x3)
#define GPIO_MODE_PIN_MASK	(0x3)
#define GPIO_MODE_PIN_SHIFT	(0x2)

#define GPIOA_OTYPER ((uint32_t *) (GPIOA_MODER + 1))
#define GPIOB_OTYPER ((uint32_t *) (GPIOB_MODER + 1))
#define GPIOC_OTYPER ((uint32_t *) (GPIOC_MODER + 1))
#define GPIOD_OTYPER ((uint32_t *) (GPIOD_MODER + 1))
#define GPIOE_OTYPER ((uint32_t *) (GPIOE_MODER + 1))
#define GPIOF_OTYPER ((uint32_t *) (GPIOF_MODER + 1))
#define GPIOG_OTYPER ((uint32_t *) (GPIOG_MODER + 1))
#define GPIOH_OTYPER ((uint32_t *) (GPIOH_MODER + 1))
#define GPIOI_OTYPER ((uint32_t *) (GPIOI_MODER + 1))
#define GPIOJ_OTYPER ((uint32_t *) (GPIOJ_MODER + 1))
#define GPIOK_OTYPER ((uint32_t *) (GPIOK_MODER + 1))

#define GPIO_OTYPE_PUSHPULL	(0x0)
#define GPIO_OTYPE_OPENDRAIN	(0x1)
#define GPIO_OTYPE_PIN_MASK	(0x1)
#define GPIO_OTYPE_PIN_SHIFT	(0x1)

#define GPIOA_OSPEEDR ((uint32_t *) (GPIOA_MODER + 2))
#define GPIOB_OSPEEDR ((uint32_t *) (GPIOB_MODER + 2))
#define GPIOC_OSPEEDR ((uint32_t *) (GPIOC_MODER + 2))
#define GPIOD_OSPEEDR ((uint32_t *) (GPIOD_MODER + 2))
#define GPIOE_OSPEEDR ((uint32_t *) (GPIOE_MODER + 2))
#define GPIOF_OSPEEDR ((uint32_t *) (GPIOF_MODER + 2))
#define GPIOG_OSPEEDR ((uint32_t *) (GPIOG_MODER + 2))
#define GPIOH_OSPEEDR ((uint32_t *) (GPIOH_MODER + 2))
#define GPIOI_OSPEEDR ((uint32_t *) (GPIOI_MODER + 2))
#define GPIOJ_OSPEEDR ((uint32_t *) (GPIOJ_MODER + 2))
#define GPIOK_OSPEEDR ((uint32_t *) (GPIOK_MODER + 2))

#define GPIO_OSPEED_LOW		(0x0)
#define GPIO_OSPEED_MEDIUM	(0x1)
#define GPIO_OSPEED_HIGH	(0x2)
#define GPIO_OSPEED_VERY_HIGH	(0x3)
#define GPIO_OSPEED_PIN_MASK	(0x3)
#define GPIO_OSPEED_PIN_SHIFT	(0x2)

#define GPIOA_PUPDR ((uint32_t *) (GPIOA_MODER + 3))
#define GPIOB_PUPDR ((uint32_t *) (GPIOB_MODER + 3))
#define GPIOC_PUPDR ((uint32_t *) (GPIOC_MODER + 3))
#define GPIOD_PUPDR ((uint32_t *) (GPIOD_MODER + 3))
#define GPIOE_PUPDR ((uint32_t *) (GPIOE_MODER + 3))
#define GPIOF_PUPDR ((uint32_t *) (GPIOF_MODER + 3))
#define GPIOG_PUPDR ((uint32_t *) (GPIOG_MODER + 3))
#define GPIOH_PUPDR ((uint32_t *) (GPIOH_MODER + 3))
#define GPIOI_PUPDR ((uint32_t *) (GPIOI_MODER + 3))
#define GPIOJ_PUPDR ((uint32_t *) (GPIOJ_MODER + 3))
#define GPIOK_PUPDR ((uint32_t *) (GPIOK_MODER + 3))

#define GPIO_PUPD_NONE		(0x0)
#define GPIO_PUPD_UP		(0x1)
#define GPIO_PUPD_DOWN		(0x2)
#define GPIO_PUPD_PIN_MASK	(0x3)
#define GPIO_PUPD_PIN_SHIFT	(0x2)

#define GPIOA_IDR ((uint32_t *) (GPIOA_MODER + 4))
#define GPIOB_IDR ((uint32_t *) (GPIOB_MODER + 4))
#define GPIOC_IDR ((uint32_t *) (GPIOC_MODER + 4))
#define GPIOD_IDR ((uint32_t *) (GPIOD_MODER + 4))
#define GPIOE_IDR ((uint32_t *) (GPIOE_MODER + 4))
#define GPIOF_IDR ((uint32_t *) (GPIOF_MODER + 4))
#define GPIOG_IDR ((uint32_t *) (GPIOG_MODER + 4))
#define GPIOH_IDR ((uint32_t *) (GPIOH_MODER + 4))
#define GPIOI_IDR ((uint32_t *) (GPIOI_MODER + 4))
#define GPIOJ_IDR ((uint32_t *) (GPIOJ_MODER + 4))
#define GPIOK_IDR ((uint32_t *) (GPIOK_MODER + 4))

#define GPIOA_ODR ((uint32_t *) (GPIOA_MODER + 5))
#define GPIOB_ODR ((uint32_t *) (GPIOB_MODER + 5))
#define GPIOC_ODR ((uint32_t *) (GPIOC_MODER + 5))
#define GPIOD_ODR ((uint32_t *) (GPIOD_MODER + 5))
#define GPIOE_ODR ((uint32_t *) (GPIOE_MODER + 5))
#define GPIOF_ODR ((uint32_t *) (GPIOF_MODER + 5))
#define GPIOG_ODR ((uint32_t *) (GPIOG_MODER + 5))
#define GPIOH_ODR ((uint32_t *) (GPIOH_MODER + 5))
#define GPIOI_ODR ((uint32_t *) (GPIOI_MODER + 5))
#define GPIOJ_ODR ((uint32_t *) (GPIOJ_MODER + 5))
#define GPIOK_ODR ((uint32_t *) (GPIOK_MODER + 5))

#define GPIOA_BSRR ((uint32_t *) (GPIOA_MODER + 6))
#define GPIOB_BSRR ((uint32_t *) (GPIOB_MODER + 6))
#define GPIOC_BSRR ((uint32_t *) (GPIOC_MODER + 6))
#define GPIOD_BSRR ((uint32_t *) (GPIOD_MODER + 6))
#define GPIOE_BSRR ((uint32_t *) (GPIOE_MODER + 6))
#define GPIOF_BSRR ((uint32_t *) (GPIOF_MODER + 6))
#define GPIOG_BSRR ((uint32_t *) (GPIOG_MODER + 6))
#define GPIOH_BSRR ((uint32_t *) (GPIOH_MODER + 6))
#define GPIOI_BSRR ((uint32_t *) (GPIOI_MODER + 6))
#define GPIOJ_BSRR ((uint32_t *) (GPIOJ_MODER + 6))
#define GPIOK_BSRR ((uint32_t *) (GPIOK_MODER + 6))

#define GPIOA_LCKR ((uint32_t *) (GPIOA_MODER + 7))
#define GPIOB_LCKR ((uint32_t *) (GPIOB_MODER + 7))
#define GPIOC_LCKR ((uint32_t *) (GPIOC_MODER + 7))
#define GPIOD_LCKR ((uint32_t *) (GPIOD_MODER + 7))
#define GPIOE_LCKR ((uint32_t *) (GPIOE_MODER + 7))
#define GPIOF_LCKR ((uint32_t *) (GPIOF_MODER + 7))
#define GPIOG_LCKR ((uint32_t *) (GPIOG_MODER + 7))
#define GPIOH_LCKR ((uint32_t *) (GPIOH_MODER + 7))
#define GPIOI_LCKR ((uint32_t *) (GPIOI_MODER + 7))
#define GPIOJ_LCKR ((uint32_t *) (GPIOJ_MODER + 7))
#define GPIOK_LCKR ((uint32_t *) (GPIOK_MODER + 7))

#define GPIOA_AFRL ((uint32_t *) (GPIOA_MODER + 8))
#define GPIOB_AFRL ((uint32_t *) (GPIOB_MODER + 8))
#define GPIOC_AFRL ((uint32_t *) (GPIOC_MODER + 8))
#define GPIOD_AFRL ((uint32_t *) (GPIOD_MODER + 8))
#define GPIOE_AFRL ((uint32_t *) (GPIOE_MODER + 8))
#define GPIOF_AFRL ((uint32_t *) (GPIOF_MODER + 8))
#define GPIOG_AFRL ((uint32_t *) (GPIOG_MODER + 8))
#define GPIOH_AFRL ((uint32_t *) (GPIOH_MODER + 8))
#define GPIOI_AFRL ((uint32_t *) (GPIOI_MODER + 8))
#define GPIOJ_AFRL ((uint32_t *) (GPIOJ_MODER + 8))
#define GPIOK_AFRL ((uint32_t *) (GPIOK_MODER + 8))

#define GPIO_AFRL_PIN_MASK	(0xf)
#define GPIO_AFRL_PIN_SHIFT	(0x4)

#define GPIOA_AFRH ((uint32_t *) (GPIOA_MODER + 9))
#define GPIOB_AFRH ((uint32_t *) (GPIOB_MODER + 9))
#define GPIOC_AFRH ((uint32_t *) (GPIOC_MODER + 9))
#define GPIOD_AFRH ((uint32_t *) (GPIOD_MODER + 9))
#define GPIOE_AFRH ((uint32_t *) (GPIOE_MODER + 9))
#define GPIOF_AFRH ((uint32_t *) (GPIOF_MODER + 9))
#define GPIOG_AFRH ((uint32_t *) (GPIOG_MODER + 9))
#define GPIOH_AFRH ((uint32_t *) (GPIOH_MODER + 9))
#define GPIOI_AFRH ((uint32_t *) (GPIOI_MODER + 9))
#define GPIOJ_AFRH ((uint32_t *) (GPIOJ_MODER + 9))
#define GPIOK_AFRH ((uint32_t *) (GPIOK_MODER + 9))

#define GPIO_AFRH_PIN_MASK	(0xf)
#define GPIO_AFRH_PIN_SHIFT	(0x4)

#define GPIO_AF00	0
#define GPIO_AF01	1
#define GPIO_AF02	2
#define GPIO_AF03	3
#define GPIO_AF04	4
#define GPIO_AF05	5
#define GPIO_AF06	6
#define GPIO_AF07	7
#define GPIO_AF08	8
#define GPIO_AF09	9
#define GPIO_AF10	10
#define GPIO_AF11	11
#define GPIO_AF12	12
#define GPIO_AF13	13
#define GPIO_AF14	14
#define GPIO_AF15	15

#define GPIO_PIN_00_DATA_OUT (1 << 0)
#define GPIO_PIN_01_DATA_OUT (1 << 1)
#define GPIO_PIN_02_DATA_OUT (1 << 2)
#define GPIO_PIN_03_DATA_OUT (1 << 3)
#define GPIO_PIN_04_DATA_OUT (1 << 4)
#define GPIO_PIN_05_DATA_OUT (1 << 5)
#define GPIO_PIN_06_DATA_OUT (1 << 6)
#define GPIO_PIN_07_DATA_OUT (1 << 7)
#define GPIO_PIN_08_DATA_OUT (1 << 8)
#define GPIO_PIN_09_DATA_OUT (1 << 9)
#define GPIO_PIN_10_DATA_OUT (1 << 10)
#define GPIO_PIN_11_DATA_OUT (1 << 11)
#define GPIO_PIN_12_DATA_OUT (1 << 12)
#define GPIO_PIN_13_DATA_OUT (1 << 13)
#define GPIO_PIN_14_DATA_OUT (1 << 14)
#define GPIO_PIN_15_DATA_OUT (1 << 15)

#define GPIO_PIN_MIN 0
#define GPIO_PIN_MAX 15

#define GPIO_PIN_AFLR_MAX 7 /* Set AF to use AFHR if PIN > GPIO_AFLR_MAX */

int gpio_set_mode(uint32_t *reg, int pin, int mode);
int gpio_set_otype(uint32_t *reg, int pin, int type);
int gpio_set_ospeed(uint32_t *reg, int pin, int speed);
int gpio_set_pupd(uint32_t *reg, int pin, int pupd);
int gpio_set_af_low(uint32_t *reg, int pin, int afl);
int gpio_set_af_high(uint32_t *reg, int pin, int afh);
int gpio_set_af(uint32_t *reg, int pin, int af);
#endif /* GPIO_H */
