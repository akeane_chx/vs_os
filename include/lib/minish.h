#ifndef __MINISH_H
#define __MINISH_H

#include <vsos/fs.h>

#define MINISH_MAX_ARGC 8

#define MINISH_ARG_MAX FS_NAME_MAX
#define MINISH_CMD_MAX FS_NAME_MAX

struct minish_arg {
	char arg[MINISH_ARG_MAX];
};

struct minish_cmd {
	char name[MINISH_CMD_MAX];
	int (*func)(int argc, char *argv[]);
};

int minish(char *user_name);
int minish_ls(int argc, char *argv[]);
int minish_cat(int argc, char *argv[]);
int minish_mount(int argc, char *argv[]);
int minish_pwd(int argc, char *argv[]);
#endif				/* __MINI_SH */
