#ifndef __STDIO_H
#define __STDIO_H

extern int stdin_fd;
extern int stdout_fd;
extern int stderr_fd;

int putchar(int c);
int puts(const char *str);
int puti(int val);
int getc(void);
int gets(char *str, int size);
int printf(const char *format, ...);
#endif				/* __STDIO_H */
