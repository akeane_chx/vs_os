#ifndef __DIRENT_H
#define __DIRENT_H

#include <vsos/fs.h>

typedef struct DIR {
	int fd;
	struct fs_dirent dirent;
} DIR;

DIR *opendir(const char *name);
DIR *fdopendir(int fd);
int closedir(DIR *dirp);
struct fs_dirent *readdir(DIR *dirp);
#endif				/* __DIRENT_H */
