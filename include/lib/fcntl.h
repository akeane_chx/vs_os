#ifndef __FCNTL_H
#define __FCNTL_H

#define O_RDONLY 0
#define O_WRONLY 1
#define O_RDWR	2

int open(const char *pathname, int flags);
#endif	/* __FCNTL_H */
