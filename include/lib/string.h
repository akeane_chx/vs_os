#ifndef __STRING_H
#define __STRING_H

#include <lib/stdlib.h>

int strneq(const char *str1, const char *str2, size_t n);
void *memset(void *s, int c, size_t n);
void *memcpy(void *d, void *s, size_t n);
char *strncpy(char *d, const char *s, size_t n);
char *strncat(char *dest, const char *src, size_t n);
int strnlen(const char *s, size_t maxlen);
void itoa(char *str, int val);
void xtoa(char *str, int val);
#endif				/* __STRING_H */
