#ifndef __UNISTD_H
#define __UNISTD_H
int read(int fd, void *buf, int count);
int write(int fd, void *buf, int count);
char *getcwd(char *buf, int size);
int close(int fd);
#endif				/* __UNISTD_H */
