#include <lib/unistd.h>
#include <stdint.h>
#include <lib/stdio.h>
#include <lib/string.h>
#include <vsos/errno.h>
#include <vsos/block.h>
#include <vsos/vsfs.h>
#include <vsos/fs.h>

int vsfs_dirent_to_fs(struct fs_dirent *fs_dirent, struct vsfs_dirent *dirent)
{
	struct vsfs_inode *inode;
	inode = &(dirent->inode);

	fs_dirent->inode.perm = inode->perm;
	fs_dirent->inode.len = inode->len;
	fs_dirent->inode.block = inode->block;

	switch(inode->type) {
		case VSFS_TYPE_BLOCK:
			fs_dirent->inode.type = FS_TYPE_BLOCK;
			fs_dirent->inode.major = inode->major;
			fs_dirent->inode.minor = inode->minor;
			fs_dirent->inode.block = -1;
			break;
		case VSFS_TYPE_CHAR:
			fs_dirent->inode.type = FS_TYPE_CHAR;
			fs_dirent->inode.major = inode->major;
			fs_dirent->inode.minor = inode->minor;
			fs_dirent->inode.block = -1;
			break;
		case VSFS_TYPE_DIR:
			fs_dirent->inode.type = FS_TYPE_DIR;
			break;
		case VSFS_TYPE_DIREXT:
			fs_dirent->inode.type = FS_TYPE_DIREXT;
			break;
		case VSFS_TYPE_REG:
			fs_dirent->inode.type = FS_TYPE_REG;
			break;
		case VSFS_TYPE_REGEXT:
			fs_dirent->inode.type = FS_TYPE_REGEXT;
			break;
		default:
		return -EINVAL;
	}

	strncpy(fs_dirent->name, dirent->name, FS_NAME_MAX);
	return 0;
}

int vsfs_mount(int bdev, struct superblock *sb)
{
	unsigned char block[VSFS_BLOCK_SIZE];
	struct vsfs_superblock *superblock;
	struct block_dev *block_dev = block_dev_get(bdev);

	if(!block_dev)
		return -ENODEV;

	block_dev->open();
	block_dev->read(0, block);
	
	superblock = (struct vsfs_superblock *) &(block[0]);
	
	if(!(superblock->id == VSFS_ID))
                return -EINVAL;

	//printf("root inode located at block: %d\n", (int)superblock->i_root.block);
       
	sb->id = VSFS_ID;
	sb->blocks = superblock->blocks;
	sb->block_size = superblock->block_size;
	sb->i_root.bdev = bdev;
        sb->i_root.type = superblock->i_root.type;
        sb->i_root.major = 0;
        sb->i_root.minor = 0;
        sb->i_root.len = superblock->i_root.len;
        sb->i_root.perm = superblock->i_root.perm;
        sb->i_root.block = superblock->i_root.block;
	
	return 0;
}

int vsfs_open(struct fs_file *file, int flags)
{
	file->pos = 0;
	file->dentries = 0;
	file->state = FS_FILE_IN_USE;

	return 0;
}

int vsfs_read(struct fs_file *file, void *buf, int len)
{
	char block[VSFS_BLOCK_SIZE];
	char *copy_addr = (char *) ((int) block + file->pos);
	int max_copy_len = 0;
	int copy_len = 0;
	struct block_dev *block_dev = 0;

	block_dev = block_dev_get(file->inode.bdev);

	if(!block_dev)
		return -ENODEV;

	if(file->pos >= file->inode.len) {
		/* EOF */
		return 0;
	}

	max_copy_len = file->inode.len - file->pos;

	if(len > max_copy_len) 
		copy_len = max_copy_len;
	else
		copy_len = len;
	
	//puts("vsfs_read\n");
	block_dev->read(file->inode.block, block); 
	memcpy(buf, copy_addr, copy_len);
	file->pos += copy_len;
	
	return len;
}

int vsfs_readdir(struct fs_file *file, struct fs_dirent *dirent)
{
	int ret = 0;
	uint8_t entries = 0;
	struct vsfs_dirent vsfs_dirent;

	if(file->inode.type != FS_TYPE_DIR)
		return -ENOTDIR;

	if(file->pos == 0) {
		//puts("reading entries : ");
		vsfs_read(file, &entries, sizeof(entries));		
		//puti(entries);
		//puts("\n");
		file->dentries = entries;
	}

	/* No more entries to read */
	if(!file->dentries) {
		return 0;
	}

	/* Read an entry */
	if((ret = vsfs_read(file, &vsfs_dirent, sizeof(struct vsfs_dirent)))) {
		vsfs_dirent_to_fs(dirent, &vsfs_dirent);
		file->dentries--;
	}
	
	return ret;
}


/*
can return EIO    An I/O error occurred.
*/
int vsfs_close(struct fs_file *file)
{
	//puts("vsfs_close\n");
	file->pos = 0;	
	file->dentries = 0;
	file->name[0] = 0;
	file->state = FS_FILE_AVAILABLE;
	return 0;
}

struct fs vsfs_fs = {
	.name = "vsfs",
	.id = VSFS_ID,
	.ops.open = vsfs_open,
	.ops.close = vsfs_close,
	.ops.read = vsfs_read,
	.ops.mount = vsfs_mount,
	.ops.readdir = vsfs_readdir,
};

int vsfs_init(void)
{
	return fs_register(&vsfs_fs);	
}
