#include <lib/stdio.h>
#include <lib/string.h>
#include <vsos/fs.h>
#include <vsos/block.h>
#include <vsos/printk.h>

static int fs_num;
static struct fs *fs[FS_MAX];

int fs_init()
{
	return 0;
}

struct fs *fs_get(char *name)
{
	int i = 0;
	
	for(i = 0; i < FS_MAX; i++) {
		if(strneq(name, fs[i]->name, FS_NAME_MAX))
			return fs[i];
	}	

	return 0;
}

int fs_register(struct fs *f)
{
	if(fs_num >= FS_MAX)
		return -1;

	fs[fs_num] = f;
	printk("Filesystem: %s: 0x%x registered\n", f->name, f->id);
	fs_num++;
	
	return 0;	
}

int fs_unregister(struct fs *f)
{
	return 0;
}
