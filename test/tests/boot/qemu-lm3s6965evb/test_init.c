#include <lib/string.h>
#include <lib/stdio.h>
#include <lib/utmp.h>
#include <vsos/task.h>
#include <vsos/char.h>
#include <vsos/block.h>
#include <vsos/mount.h>
#include <vsos/vsfs.h>
#include <vsos/fs.h>
#include <vsos/flash.h>
#include <vsos/board.h>
#include <vsos/sections.h>
#include <vsos/printk.h>
#include <vsos/timer.h>

#define BAUD_RATE 115200

void test_start(void)
{
	printk("TEST START");
	return;
}

void test_end(void)
{
	printk("TEST END\n");
	while(1) {;}
}

int test_init(void)
{
	struct board *board = board_init();

	board->serial_setup(BAUD_RATE);

	test_start();
	printk("\n\nVSOS BOOT TEST\n\n");
	printk("Board: %s\n", board->name);	
	printk("Serial baudrate: %d\n", BAUD_RATE);
	printk("Clock rate : %d\n", board->clock_rate);

	test_end();

	return 0;
}
