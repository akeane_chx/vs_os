#/bin/bash

boot_openocd () {
	BOARD_BOOT="$1"
	TEST_NAME="$2"

	export BOARD=$BOARD_BOOT
	export BOARD_TEST=$TEST_NAME

	# Check openocd
	if ! ps waux | grep openocd | grep cfg
	then
		make -C openocd 2> /dev/null > /dev/null &
		OCD_PID=$!
		sleep 5
	fi

	export BOARD=$BOARD_BOOT

	# run 'make clean'
	if ! make clean
	then
		return 1
	fi

	# run 'make to build vsos.elf'
	if ! make test_debug $TEST_NAME
	then
		return 1
	fi

	kill $OCD_PID
}

boot_qemu () {
	BOARD_BOOT="$1"
	TEST_NAME="$2"

	export BOARD=$BOARD_BOOT
	export BOARD_TEST=$TEST_NAME

	# run 'make clean'
	if ! make clean
	then
		return 1
	fi

	# run 'make to build vsos.elf'
	if ! make test_debug $TEST_NAME
	then
		return 1
	fi

	kill $OCD_PID
}

boot_openocd stm32p405 boot
boot_qemu qemu-lm3s6965evb boot

exit 0
