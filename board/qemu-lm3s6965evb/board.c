#include <lib/string.h>
#include <vsos/sections.h>
#include <vsos/board.h>
#include <qemu-lm3s6965evb/serial.h>
#include <qemu-lm3s6965evb/timer.h>
#include <qemu-lm3s6965evb/rcc.h>
#include <qemu-lm3s6965evb/led.h>

#define BOARD_NAME "lm3s6965evb"

static struct board board; 

struct board *board_init(void)
{
	board.bss_start = &__bss_start;
	board.bss_end = &__bss_end;
	board.bss_size = ((unsigned int)&__bss_end) - ((unsigned int)&__bss_start);
	memset(board.bss_start, 0, board.bss_size);
	/* These need to be reinitialised as the memset zeros them */
	board.bss_start = &__bss_start;
	board.bss_end = &__bss_end;
	board.bss_size = ((unsigned int)&__bss_end) - ((unsigned int)&__bss_start) - 1;

	board.main_sp = __main_sp; /* main stack pointer */
	board.data_size = ((unsigned int)&__data_end) - ((unsigned int)&__data_vma) - 1;
	board.data_vma = &__data_vma;
	board.data_lma = &__data_lma;
	board.data_end = &__data_end;
	board.flash_fs_start = &__flash_fs_start;
	board.flash_fs_end = &__flash_fs_end;

	board.clock_hsi = RCC_HSI_MHZ;
	board.clock_hse = 0;
	board.clock_rate = board.clock_hsi;

	strncpy(&board.name[0], BOARD_NAME, BOARD_NAME_MAX);

	board.serial_setup = _serial_setup;
	board.putchar = _serial_putchar;	
	board.getc = _serial_getc;
	board.led_setup = _led_setup;
	board.led_on = _led_on;
	board.led_off = _led_off;
	board.timer_start = _timer_start;
	board.timer_stop = _timer_stop;
	board.timer_reload = _timer_reload;	
	memcpy(board.data_vma, board.data_lma, board.data_size);

	return &board;
}

struct board *board_get(void)
{
	return &board;
}
