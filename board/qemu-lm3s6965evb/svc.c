#include <vsos/irq.h>

int svc_ret;
int svc_in;

__attribute__ ((interrupt))
void svc_handler(void)
{
	irq_disable();
	svc_in = 1;

	/* arg * of syscall points to psp */
	asm volatile ("mrs r0, psp");
	asm volatile ("bl syscall");

	/* Save the ret val */
	asm volatile ("mov %[r], r0":[r] "=r"(svc_ret));

	svc_in = 0;
	irq_enable();
	
	/* Return from exception using PSP */
	asm volatile ("mov lr, #0xfffffffd");
}
