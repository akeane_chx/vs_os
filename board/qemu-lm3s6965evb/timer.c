#include <stdint.h>
#include <vsos/bitops.h>
#include <vsos/task.h>
#include <qemu-lm3s6965evb/timer.h>

void _timer_start()
{
	reg_set(SYSTICK_CTRL, SYSTICK_CTRL_ACTIVATE);
}

void _timer_stop()
{
        reg_unset_bits(SYSTICK_CTRL, SYSTICK_CTRL_ACTIVATE);
}

void _timer_reload(unsigned int reload)
{
	reg_set(SYSTICK_TRELOAD, (uint32_t) reload);
}

__attribute__((interrupt)) void _timer_func()
{
	timer_func();
}
