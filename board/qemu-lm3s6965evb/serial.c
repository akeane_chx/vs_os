#include <vsos/errno.h>
#include <stdint.h>
#include <qemu-lm3s6965evb/serial.h>
#include <vsos/bitops.h>

int _serial_setup(int baud_rate)
{
	return 0;
}

int _serial_putchar(int c)
{
	while ((*UART0_FR & UART_FR_TXFF))
		;

	reg_set(UART0_DR, (char) c);

	if ((char)c == '\n') {
		while ((*UART0_FR & UART_FR_TXFF))
			;
		reg_set(UART0_DR, '\r');
	}

	if ((char)c == '\r') {
		while ((*UART0_FR & UART_FR_TXFF))
			;

		reg_set(UART0_DR, '\n');
	}

	return 0;
}

void _serial_puts(unsigned char *str)
{
	int i = 0;

	while (str[i]) {
		_serial_putchar((int)str[i]);
		i++;
	}
}

int _serial_getc(void)
{
	if ((*UART0_FR & UART_FR_RXFE))
		return -EAGAIN;	

	return *UART0_DR & 0xff;
}

int _serial_gets(char *str, int size, int echo)
{
	int i = 0;

	while (i < size) {
		str[i] = (char)_serial_getc();
		if (str[i] == '\r') {
			str[i] = 0;
			return i;
		}
		i++;
	}

	str[i] = 0;
	return i;
}
