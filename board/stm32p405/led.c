#include <vsos/errno.h>
#include <stdint.h>
#include <vsos/bitops.h>
#include <stm32p405/gpio.h>
#include <stm32p405/rcc.h>
#include <stm32p405/serial.h>

void g_delay(int cycles)
{
        while(cycles) {
                cycles--;
        }

}

int _led_setup(int led_mask)
{
	/* Enable GPIOC Clock */
	reg_set_bits(RCC_AHB1ENR, RCC_GPIO_C_EN);

	g_delay(100000);
	
	/* Set GPIO pin to GPO */
	gpio_set_mode(GPIOC_MODER, 12, GPIO_MODE_GPO);

	return 0;
}

int _led_on(int led)
{
	reg_set_bits(GPIOC_ODR, GPIO_PIN_12_DATA_OUT);
	return 0;	
}

int _led_off(int led)
{
	reg_unset_bits(GPIOC_ODR, GPIO_PIN_12_DATA_OUT);
	return 0;	
}
