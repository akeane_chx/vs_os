#include <vsos/bitops.h>
#include <stm32p405/gpio.h>

int gpio_set_mode(uint32_t *reg, int pin, int mode)
{
	uint32_t shift = (pin * GPIO_MODE_PIN_SHIFT);
	uint32_t bits = (mode & GPIO_MODE_PIN_MASK) << shift;
	uint32_t mask = GPIO_MODE_PIN_MASK << shift;

	reg_unset_bits(reg, mask);
	reg_set_bits(reg, bits);

	return 0;
}

int gpio_set_otype(uint32_t *reg, int pin, int type)
{
	uint32_t shift = (pin * GPIO_OTYPE_PIN_SHIFT);
	uint32_t bits = (type & GPIO_OTYPE_PIN_MASK) << shift;
	uint32_t mask = GPIO_OTYPE_PIN_MASK << shift;

	reg_unset_bits(reg, mask);
	reg_set_bits(reg, bits);

	return 0;
}

int gpio_set_ospeed(uint32_t *reg, int pin, int speed)
{
	uint32_t shift = (pin * GPIO_OSPEED_PIN_SHIFT);
	uint32_t bits = (speed & GPIO_OSPEED_PIN_MASK) << shift;
	uint32_t mask = GPIO_OSPEED_PIN_MASK << shift;

	reg_unset_bits(reg, mask);
	reg_set_bits(reg, bits);

	return 0;
}

int gpio_set_pupd(uint32_t *reg, int pin, int pupd)
{
	uint32_t shift = (pin * GPIO_PUPD_PIN_SHIFT);
	uint32_t bits = (pupd & GPIO_PUPD_PIN_MASK) << shift;
	uint32_t mask = GPIO_PUPD_PIN_MASK << shift;

	reg_unset_bits(reg, mask);
	reg_set_bits(reg, bits);

	return 0;
}

int gpio_set_af_low(uint32_t *reg, int pin, int afl)
{
	uint32_t shift = (pin * GPIO_AFRL_PIN_SHIFT);
	uint32_t bits = (afl & GPIO_AFRL_PIN_MASK) << shift;
	uint32_t mask = GPIO_AFRL_PIN_MASK << shift;

	reg_unset_bits(reg, mask);
	reg_set_bits(reg, bits);

	return 0;
}

int gpio_set_af_high(uint32_t *reg, int pin, int afh)
{
	uint32_t shift = (pin * GPIO_AFRH_PIN_SHIFT);
	uint32_t bits = (afh & GPIO_AFRH_PIN_MASK) << shift;
	uint32_t mask = GPIO_AFRH_PIN_MASK << shift;

	reg_unset_bits(reg, mask);
	reg_set_bits(reg, bits);

	return 0;
}

int gpio_set_af(uint32_t *reg, int pin, int af)
{
	if (pin > GPIO_PIN_AFLR_MAX) {
		pin = pin - GPIO_PIN_AFLR_MAX + 1;
		return gpio_set_af_high(reg + 1, pin, af);
	} else
		return gpio_set_af_low(reg, pin, af);

}
