#include <stm32p405/fpu.h>
#include <vsos/bitops.h>

void fpu_enable(void)
{
	reg_set_bits(FPU_CPACR, FPU_EN);
	asm("dsb");
	asm("isb");
}
