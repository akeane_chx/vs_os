#include <vsos/errno.h>
#include <stdint.h>
#include <vsos/bitops.h>
#include <stm32p405/gpio.h>
#include <stm32p405/rcc.h>
#include <stm32p405/serial.h>

void delay(int cycles)
{
        while(cycles) {
                cycles--;
        }

}

int _serial_setup(int baud_rate)
{
	/* Enable GPIOA Clock */
	reg_set_bits(RCC_AHB1ENR, RCC_GPIO_A_EN);

	/* Enable USART2 clock */
	reg_set_bits(RCC_APB1ENR, RCC_USART2_EN);
	delay(100000);
	
	/* TX init */
	/* Set TX GPIO pin to AF */
	gpio_set_mode(GPIOA_MODER, GPIO_PIN_TX, GPIO_MODE_AF);

	/* Set to push pull */
	gpio_set_otype(GPIOA_OTYPER, GPIO_PIN_TX, GPIO_OTYPE_PUSHPULL);

	/* high speed */
	gpio_set_ospeed(GPIOA_OSPEEDR, GPIO_PIN_TX, GPIO_OSPEED_HIGH);

	/* set pull-up */
	gpio_set_pupd(GPIOA_PUPDR, GPIO_PIN_TX, GPIO_PUPD_UP);

	/* Set TX AF function */
	gpio_set_af(GPIOA_AFRL, GPIO_PIN_TX, GPIO_AF07);

	/* RX init */
	/* Set RX GPIO pin to AF */
	gpio_set_mode(GPIOA_MODER, GPIO_PIN_RX, GPIO_MODE_AF);

	/* Set RX to push pull */
	gpio_set_otype(GPIOA_OTYPER, GPIO_PIN_RX, GPIO_OTYPE_PUSHPULL);

	/* high speed */
	gpio_set_ospeed(GPIOA_OSPEEDR, GPIO_PIN_RX, GPIO_OSPEED_HIGH);

	/* set pull-up */
	gpio_set_pupd(GPIOA_PUPDR, GPIO_PIN_RX, GPIO_PUPD_UP);

	/* Set AF function */
	gpio_set_af(GPIOA_AFRL, GPIO_PIN_RX, GPIO_AF07);

	/* Set BRR and enable USART TX/RX */
	reg_set(USART2_BRR, RCC_HSI_MHZ / baud_rate);
	reg_set_bits(USART2_CR1, USART_TX_EN | USART_EN | USART_RX_EN);
	return 0;
}

int _serial_putchar(int c)
{
	int recv_c = 0;
	int restore_c = 0;
#if 0
	/* Should return EBUSY if there is data waiting */
	/* maybe have FIFOS in read/write layer, so don't lose data */


	/* If there is data waiting, save it */
	if(reg_read(USART2_SR) & USART_RXNE) {
		recv_c = reg_read(USART2_DR);
		restore_c = 1;	
	}
#endif /* 0 */
	while (!(reg_read(USART2_SR) & USART_TXE))
		;

	reg_set(USART2_DR, (char)c);

	if ((char)c == '\n') {
		while (!(reg_read(USART2_SR) & USART_TXE))
			;

		reg_set(USART2_DR, '\r');
	}

	if ((char)c == '\r') {
		while (!(reg_read(USART2_SR) & USART_TXE))
			;

		reg_set(USART2_DR, '\n');
	}

	if(restore_c)
		reg_set(USART2_DR, recv_c);

	return 0;
}

int _serial_getc(void)
{
	if(!(reg_read(USART2_SR) & USART_RXNE))
		return -EAGAIN;

	return *USART2_DR & 0xff;
}
