#include <stdint.h>
#include <vsos/bitops.h>
#include <stm32p405/timer.h>
#include <vsos/task.h>
#include <vsos/syscall.h>
#include <vsos/printk.h>

void _timer_start()
{
	reg_set(SYSTICK_CTRL, SYSTICK_CTRL_ACTIVATE);
}

void _timer_stop()
{
        reg_unset_bits(SYSTICK_CTRL, SYSTICK_CTRL_ACTIVATE);
}

void _timer_reload(unsigned int reload)
{
	reg_set(SYSTICK_TRELOAD, (uint32_t) reload);
}

__attribute__((interrupt)) void _timer_func()
{
	timer_func();
#if 0
	static uint32_t no_task_switch = 10;

	if(no_task_switch) {
		no_task_switch--;
	} else {
		if(!svc_in) {
		if(task_schedule())
			/* Trigger the pendSV */
			reg_set_bits(PENDSV_BASE, PENDSV_BIT);
		}

		/* Reset the counter */
		no_task_switch = 10;
	}
#endif 
}
