#include <lib/string.h>
#include <vsos/sections.h>
#include <vsos/board.h>
#include <stm32p405/serial.h>
#include <stm32p405/timer.h>
#include <stm32p405/rcc.h>
#include <stm32p405/led.h>

#define BOARD_NAME "stm32p405"

static struct board board; 

struct board *board_init(void)
{
	uint32_t bss_size = ((unsigned int)&__bss_end) - ((unsigned int)&__bss_start);
	uint32_t data_size = ((unsigned int)&__data_end) - ((unsigned int)&__data_lma);

	/* Zero the bss */
	memset(&__bss_start, 0, bss_size);
	
	/* Copy data from flash to sram */
	memcpy(&__data_lma, &__data_vma, data_size);

	board.bss_start = &__bss_start;
	board.bss_end = &__bss_end;
	board.bss_size = bss_size;

	board.main_sp = __main_sp; /* main stack pointer */ 
	board.data_size = data_size;
	board.data_vma = &__data_vma;
	board.data_lma = &__data_lma;
	board.data_end = &__data_end;
	board.flash_fs_start = &__flash_fs_start;
	board.flash_fs_end = &__flash_fs_end;

	board.clock_hsi = RCC_HSI_MHZ;
	board.clock_hse = 0;
	board.clock_rate = board.clock_hsi;
	
	strncpy(&board.name[0], BOARD_NAME, BOARD_NAME_MAX);
	
	board.serial_setup = _serial_setup;
	board.putchar = _serial_putchar;	
	board.getc = _serial_getc;
	board.led_setup = _led_setup;
	board.led_on = _led_on;
	board.led_off = _led_off;
	board.timer_start = _timer_start;
        board.timer_stop = _timer_stop;
        board.timer_reload = _timer_reload;

	return &board;
}

struct board *board_get(void)
{
	return &board;
}
